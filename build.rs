use std::{env, fs};
use chrono::{Datelike, Timelike, Local};
use rust_utils::utils::run_command;

const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() { 
    println!("cargo:rerun-if-changed=build.rs");
    let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September","October","November","December"];
    let date = Local::now().date_naive();
    let month = months[date.month() as usize - 1];
    let day = date.day();
    let year = date.year();

    let date_str = format!("{} {} {}",day, month, year);

    let time = Local::now().time();
    let hour = time.hour();
    let minute = time.minute();
    let second = time.second(); 

    let time_str = format!("{hour:02}:{minute:02}:{second:02}");

    let servermancli_manpage = format!(
        ".\\\" Manpage for servermancli (part of Minecraft Server Manager).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\n\
        .TH servermancli 1 \"{date_str}\" \"{VERSION}\" \"servermancli man page\"\n\
        .SH NAME\n\
        servermancli \\- Command line interface for Minecraft Server Manager\n\
        .SH SYNOPSIS\n\
        servermancli setup\n\
        .br\n\
        servermancli setup-forge\n\
        .br\n\
        servermancli launch <name>\n\
        .br\n\
        servermancli restart <name>\n\
        .br\n\
        servermancli console <name>\n\
        .br\n\
        servermancli remove <name>\n\
        .br\n\
        servermancli rename <name> <new name>\n\
        .br\n\
        servermancli config <name> [option] [key] [new value]\n\
        .br\n\
        servermancli servers\n\
        .br\n\
        servermancli import\n\
        .br\n\
        servermancli version\n\n\
        .SH DESCRIPTION\n\
        Manage your Minecraft servers easily from the command line\n\n\
        .SH OPTIONS\n\
        setup: set up a new vanilla Minecraft server\n\
        .br\n\
        setup-forge: set up a new server with Minecraft Forge installed\n\
        .br\n\
        launch <name>: Launch a specified server\n\
        .br\n\
        restart <name>: Restart a specified server\n\
        .br\n\
        console <name>: Show the console of a specified server\n\
        .br\n\
        remove <name>: Deletes a specified server. Please use this carefully because it cannot be undone!\n\
        .br\n\
        rename <name> <new name>: Renames a specified server.\n\
        .br\n\
        servers: List all know Minecraft servers\n\
        .br\n\
        import: Import all servers not created by this program\n\
        .br\n\
        version: Show this program's version\n\
        .br\n\
        config <name> [option] [key] [new value]: Get or set the configuration (server.properties) of a specified server\n\
        .br\n\
        Minecraft server config options:\n\
        .br\n\
        <name> set <key name> <new value>: sets a config key to a new value\n\
        .br\n\
        <name> get <key name>: get a config key value\n\
        .br\n\
        keys: list all config keys\n\
        .SH EXAMPLES\n\
        Create a new vanilla server: servermancli setup\n\
        .br\n\
        Create a new Minecraft forge server: servermancli setup-forge\n\n\
        .SH SEE ALSO\n\
        servermand(1)\n\
        .br\n\
        serverman(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)"
    );

    let servermand_manpage = format!(
        ".\\\" Manpage for servermand (part of serverman).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH servermand 1 \"{date_str}\" \"{VERSION}\" \"servermand man page\"\n\
        .SH NAME\n\
        servermand \\- Background process for Minecraft Server Manager\n\
        .SH DESCRIPTION\n\
        This is the main process for the Minecraft Server Manager program. This process must be started for the program to be usable!\n\n\
        .SH EXAMPLES\n\
        start from systemd: systemctl start --user servermand\n\
        .br\n\
        start upon first login using systemd: systemctl enable --user servermand\n\
        .br\n\
        start manually (as background process): servermand &\n\n\
        .SH SEE ALSO\n\
        servermancli(1)\n\
        .br\n\
        serverman(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)"
    );

    let serverman_manpage = format!(
        ".\\\" Manpage for serverman (part of serverman).\n\
        .\\\" Created on {month} {day}, {year} at {time_str}\n\
        .TH serverman 1 \"{date_str}\" \"{VERSION}\" \"serverman man page\"\n\
        .SH NAME\n\
        serverman \\- Minecraft Server Manager\n\
        .SH DESCRIPTION\n\
        This is the text interface for the Minecraft Server Manager. It can be used to view server consoles, crash reports, and start/stop servers.\n\n\
        .SH SEE ALSO\n\
        servermancli(1)\n\
        .br\n\
        serverman(1)\n\n\
        .SH AUTHOR\n\
        Noah Jelen (noahtjelen@gmail.com)"
    );
    fs::remove_file("man/servermancli.1").unwrap_or(());
    fs::remove_file("man/servermand.1").unwrap_or(());
    fs::remove_file("man/serverman.1").unwrap_or(());
    fs::remove_dir("man/").unwrap_or(());

    fs::create_dir("man/").unwrap_or(());
    fs::write("man/servermancli.1", servermancli_manpage).unwrap();
    fs::write("man/servermand.1", servermand_manpage).unwrap();
    fs::write("man/serverman.1", serverman_manpage).unwrap();

    // build the Arch PKGBUILD file if the aur directory is present
    if env::set_current_dir("aur/").is_ok() {
        let aur_pkgbuild = format!(
            "# Maintainer: Noah Jelen <noahtjelen@gmail.com>\n\
            # Generated by cargo on {month} {day}, {year} at {time_str}\n\
            pkgname=serverman\n\
            pkgver={VERSION}\n\
            pkgrel=1\n\
            pkgdesc=\"Minecraft Server Management program\"\n\
            arch=(\'i686' 'x86_64\')\n\
            url=\"https://gitlab.com/NoahJelen/serverman\"\n\
            license=(\'GPL\')\n\
            depends=(\'ncurses\' \'gcc-libs\' \'glibc\')\n\
            makedepends=(\'cargo\' \'gzip\')\n\
            source=(\"https://gitlab.com/NoahJelen/serverman/-/archive/$pkgver/serverman-$pkgver.zip\")\n\
            md5sums=(\'SKIP\')\n\n\
            build() {{\n    \
                cd \"serverman-$pkgver\"\n    \
                cargo build --release\n    \
                cd target/release\n    \
                ln -sf servermand serverman\n    \
                ln -sf servermand servermancli\n\
            }}\n\n\
            package() {{\n    \
                cd \"serverman-$pkgver\"\n    \
                mkdir -p \"$pkgdir/usr/share/man/man1/\"\n    \
                mkdir -p \"$pkgdir/usr/lib/systemd/user\"\n    \
                mkdir -p \"$pkgdir/usr/share/serverman/\"\n    \
                install -Dt \"$pkgdir/usr/bin\" -m755 target/release/serverman\n    \
                install -Dt \"$pkgdir/usr/bin\" -m755 target/release/servermancli\n    \
                install -Dt \"$pkgdir/usr/bin\" -m755 target/release/servermand\n    \
                install -Dt \"$pkgdir/usr/share/man/man1\" man/servermancli.1\n    \
                install -Dt \"$pkgdir/usr/share/man/man1\" man/servermand.1\n    \
                install -Dt \"$pkgdir/usr/share/man/man1\" man/serverman.1\n    \
                install -Dt \"$pkgdir/usr/lib/systemd/user\" servermand.service\n    \
                install -Dt \"$pkgdir/usr/share/serverman\" icon.png\n\
            }}"
        );

        fs::write("PKGBUILD", aur_pkgbuild).unwrap();

        // use makepkg to generate a .SRCINFO file from the aur pkgbuild
        let cmd = run_command("makepkg", false, ["--printsrcinfo"]);
        fs::write(".SRCINFO", cmd.output).unwrap();
    }
}