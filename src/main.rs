use std::env;
use rust_utils::{utils, logging::Log};
use lazy_static::lazy_static;

mod daemon;
mod mcserver;
mod tui;
mod client;
mod cli;
mod config;

//TODO: Minecraft 1.19.4 support once fabric, forge, and no chat reports support it

lazy_static! {
    static ref LOG: Log = Log::get(utils::get_execname().as_str(), "serverman");
    static ref DEBUG: bool = should_debug();
}

const EULA_URL: &str = "https://www.minecraft.net/en-us/eula";
const VERSION: &str = env!("CARGO_PKG_VERSION");

fn main() {
    LOG.report_panics(true);
    match utils::get_execname().as_str() {
        "servermancli" => {
            let args: Vec<String> = env::args().collect();
            cli::parse_args(args);
        }

        "servermand" => daemon::init(),
        "serverman" => tui::init(),
        _ => unreachable!()
    }
}

// are we in debug mode?
fn should_debug() -> bool {
    // get the runtime arguments
    let args: Vec<String> = env::args().collect();

    // should debug messages be shown?
    if args.is_empty() { return false; }
    
    for arg in args {
        if arg.as_str() == "-d" {
            return true;
        }
    }
    false
}