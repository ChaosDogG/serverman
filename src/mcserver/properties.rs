use std::{
    fs::File,
    collections::HashMap,
    io::{BufReader, BufWriter},
    fmt::{
        Display,
        Formatter,
        Result as FmtResult
    }
};
use rand::{Rng, rngs::ThreadRng};
use colored::{ColoredString, Colorize};
use super::{
    GameMode,
    Version,
    //WorldType,
    Difficulty
};

#[derive(Clone)]
pub struct ServerProperties {
    // the raw server.properties data
    raw_properties: HashMap<String, String>,

    // the minecraft version of the server properties
    //version: Version,

    // the path of the file
    path: String,

    // cached values
    allow_flight: bool,
    difficulty: Difficulty,
    enable_command_block: bool,
    gamemode: GameMode,
    hardcore: bool,
    //level_seed: String,
    //level_type: WorldType,
    max_players: usize,
    motd: String,
    player_idle_timeout: usize,
    pvp: bool,
    port: u16,
    spawn_animals: bool,
    spawn_monsters: bool,
    spawn_npcs: bool,
    spawn_protection: usize,
    view_distance: usize,
    max_tick_time: isize
}

impl ServerProperties {
    pub fn new(path: &str, version: Version) -> ServerProperties {
        let path = format!("{path}/server.properties");
        if let Ok(prop_file) = File::open(&path) {
            let raw_properties = java_properties::read(BufReader::new(prop_file)).unwrap();
            ServerProperties {
                raw_properties: raw_properties.clone(),
                path,
                //version,

                allow_flight: raw_properties["allow-flight"].parse().unwrap_or(false),
                hardcore: raw_properties["hardcore"].parse().unwrap_or(false),
                difficulty: Difficulty::from(&raw_properties["difficulty"]),
                enable_command_block: raw_properties["enable-command-block"].parse().unwrap_or(true),
                gamemode: GameMode::from(&raw_properties["difficulty"]),
                //level_seed: raw_properties["level-seed"].to_string(),
                //level_type: WorldType::from(&raw_properties["level-type"]),
                max_players: raw_properties["max-players"].parse().unwrap_or(20),
                motd: raw_properties["motd"].to_string(),
                player_idle_timeout: raw_properties["player-idle-timeout"].parse().unwrap_or(0),
                pvp: raw_properties["pvp"].parse().unwrap_or(true),
                port: raw_properties["server-port"].parse().unwrap_or(25565),
                spawn_animals: raw_properties["spawn-animals"].parse().unwrap_or(true),
                spawn_monsters: raw_properties["spawn-monsters"].parse().unwrap_or(true),
                spawn_npcs: raw_properties["spawn-npcs"].parse().unwrap_or(true),
                spawn_protection: raw_properties["spawn-protection"].parse().unwrap_or(16),
                view_distance: raw_properties["view-distance"].parse().unwrap_or(10),
                max_tick_time: raw_properties["max-tick-time"].parse().unwrap_or(-1)
            }
        }
        else {
            let mut rand = ThreadRng::default();
            let seed: i128 = rand.gen();
            //let level_type_str = WorldType::Default.get_string(version);
            let w_type_str = if version.major <=1.12 { "NORMAL" } else { "default"};
            let mut raw_properties = HashMap::new();
            raw_properties.insert("allow-flight".to_string(), false.to_string());
            raw_properties.insert("hardcore".to_string(), false.to_string());
            raw_properties.insert("difficulty".to_string(), Difficulty::Normal.to_string());
            raw_properties.insert("enable-command-block".to_string(), true.to_string());
            raw_properties.insert("gamemode".to_string(), GameMode::Survival.to_string());
            raw_properties.insert("level-seed".to_string(), seed.to_string());
            //raw_properties.insert("level-type".to_string(), level_type_str);
            raw_properties.insert("level-type".to_string(), w_type_str.to_string());
            raw_properties.insert("max-players".to_string(), 20.to_string());
            raw_properties.insert("motd".to_string(), "A Minecraft Server".to_string());
            raw_properties.insert("player-idle-timeout".to_string(), 0.to_string());
            raw_properties.insert("pvp".to_string(), true.to_string());
            raw_properties.insert("query.port".to_string(), 25565.to_string());
            raw_properties.insert("server-port".to_string(), 25565.to_string());
            raw_properties.insert("spawn-animals".to_string(), true.to_string());
            raw_properties.insert("spawn-monsters".to_string(), true.to_string());
            raw_properties.insert("spawn-npcs".to_string(), true.to_string());
            raw_properties.insert("spawn-protection".to_string(), 16.to_string());
            raw_properties.insert("view-distance".to_string(), 10.to_string());
            raw_properties.insert("max-tick-time".to_string(), (-1).to_string());

            raw_properties.insert("allow-nether".to_string(), true.to_string());
            raw_properties.insert("broadcast-console-to-ops".to_string(), true.to_string());
            raw_properties.insert("broadcast-rcon-to-ops".to_string(), true.to_string());
            raw_properties.insert("enable-jmx-monitoring".to_string(), false.to_string());
            raw_properties.insert("enable-query".to_string(), true.to_string());
            raw_properties.insert("enable-rcon".to_string(), false.to_string());
            raw_properties.insert("enable-status".to_string(), true.to_string());
            raw_properties.insert("enforce-whitelist".to_string(), false.to_string());
            raw_properties.insert("entity-broadcast-range_percentage".to_string(), 100.to_string());
            raw_properties.insert("force-gamemode".to_string(), false.to_string());
            raw_properties.insert("function-permission-level".to_string(), 2.to_string());
            raw_properties.insert("generate-structures".to_string(), true.to_string());
            raw_properties.insert("generator-settings".to_string(), String::new());
            raw_properties.insert("level-name".to_string(), "world".to_string());
            raw_properties.insert("max-build-height".to_string(), 256.to_string());
            raw_properties.insert("max-world-size".to_string(), 29999984.to_string());
            raw_properties.insert("network-compression-threshold".to_string(), 256.to_string());
            raw_properties.insert("online-mode".to_string(), true.to_string());
            raw_properties.insert("op-permission-level".to_string(), 4.to_string());
            raw_properties.insert("prevent-proxy-connections".to_string(), false.to_string());
            raw_properties.insert("rate-limit".to_string(), 0.to_string());
            raw_properties.insert("rcon-password".to_string(), String::new());
            raw_properties.insert("rcon-port".to_string(), 25575.to_string());
            raw_properties.insert("resource-pack".to_string(), String::new());
            raw_properties.insert("resource-pack-sha1".to_string(), String::new());
            raw_properties.insert("server-ip".to_string(), "0.0.0.0".to_string());
            raw_properties.insert("snooper-enabled".to_string(), false.to_string());
            raw_properties.insert("sync-chunk-writes".to_string(), true.to_string());
            raw_properties.insert("text-filtering-config".to_string(), String::new());
            raw_properties.insert("use-native-transport".to_string(), true.to_string());
            raw_properties.insert("white-list".to_string(), false.to_string());

            if version.major == 1.19 {
                // haha Microshaft!
                raw_properties.insert("enforce-secure-profile".to_string(), false.to_string());
            }

            ServerProperties {
                raw_properties,
                path,
                //version,

                allow_flight: false,
                hardcore: false,
                difficulty: Difficulty::Normal,
                enable_command_block: true,
                gamemode: GameMode::Survival,
                //level_seed: seed.to_string(),
                //level_type: WorldType::Default,
                max_players: 20,
                motd: "A Minecraft Server".to_string(),
                player_idle_timeout: 0,
                pvp: true,
                port: 25565,
                spawn_animals: true,
                spawn_monsters: true,
                spawn_npcs: true,
                spawn_protection: 16,
                view_distance: 10,
                max_tick_time: -1
            }
        }
    }

    pub fn get_value(&self, key: &str) -> Option<&str> {
        self.raw_properties.get(key).map(|s| s.as_str())
    }

    pub fn get_disp_value(&self, key: &str) -> Option<String> {
        let val = self.get_value(key)?;
        let val = match key {
            "entity-broadcast-range-percentage" => format!("{val}%"),
            "spawn-protection" | "view-distance" => format!("{val} Chunks"),
            v => v.to_string()
        };

        Some(format!("{} {val}", get_disp_title(key)))
    }

    pub fn set_value<V: Display>(&mut self, key: &str, val: V) {
        self.raw_properties.insert(key.to_string(), val.to_string());
    }

    pub fn set_flight(&mut self, flight: bool) {
        self.set_value("allow-flight", flight);
        self.allow_flight = flight;
    }

    pub fn flight(&self) -> bool { self.allow_flight }

    pub fn allow_cmd_blocks(&mut self, allow: bool) {
        self.set_value("enable-command-block", allow);
        self.enable_command_block = allow;
    }

    pub fn cmd_blocks(&self) -> bool { self.enable_command_block }

    pub fn set_hardcore(&mut self, hardcore: bool) {
        self.set_value("hardcore", hardcore);
        self.hardcore = hardcore;
    }

    pub fn hardcore(&self) -> bool { self.hardcore }

    pub fn set_difficulty(&mut self, difficulty: Difficulty) {
        self.set_value("difficulty", difficulty);
        self.difficulty = difficulty;
    }

    pub fn difficulty(&self) -> Difficulty { self.difficulty }

    pub fn set_gamemode(&mut self, mode: GameMode) {
        self.set_value("gamemode", mode);
        self.gamemode = mode;
    }

    pub fn gamemode(&self) -> GameMode { self.gamemode }

    /*pub fn set_world_type(&mut self, w_type: WorldType) {
        self.set_value("level-type", w_type.get_string(self.version));
        self.level_type = w_type;
    }

    pub fn world_type(&self) -> &WorldType { &self.level_type }*/

    pub fn set_max_players(&mut self, players: usize) {
        self.set_value("max-players", players);
        self.max_players = players;
    }

    pub fn max_players(&self) -> usize { self.max_players }

    pub fn set_message(&mut self, msg: &str) {
        self.motd = msg.to_string();
        self.set_value("motd", msg);
    }

    pub fn message(&self) -> &str { &self.motd }

    pub fn set_player_idle_time(&mut self, time: usize) {
        self.set_value("player-idle-timeout", time);
        self.player_idle_timeout = time;
    }

    pub fn player_idle_time(&self) -> usize { self.player_idle_timeout }

    pub fn set_pvp(&mut self, pvp: bool) {
        self.set_value("pvp", pvp);
        self.pvp = pvp;
    }

    pub fn pvp(&self) -> bool { self.pvp }

    pub fn set_port(&mut self, port: u16) {
        self.set_value("server-port", port);
        self.set_value("query.port", port);
        self.port = port;
    }

    pub fn port(&self) -> u16 { self.port }

    pub fn set_spawn_animals(&mut self, enable: bool) {
        self.set_value("spawn-animals", enable);
        self.spawn_animals = enable;
    }

    pub fn set_spawn_monsters(&mut self, enable: bool) {
        self.set_value("spawn-monsters", enable);
        self.spawn_monsters = enable;
    }

    pub fn set_spawn_npcs(&mut self, enable: bool) {
        self.set_value("spawn-npcs", enable);
        self.spawn_npcs = enable;
    }

    pub fn spawn_animals(&self) -> bool { self.spawn_animals }
    pub fn spawn_monsters(&self) -> bool { self.spawn_monsters }
    pub fn spawn_npcs(&self) -> bool { self.spawn_npcs }

    pub fn set_spawn_protection_radius(&mut self, radius: usize) {
        self.set_value("spawn-protection", radius);
        self.spawn_protection = radius;
    }

    pub fn spawn_protection_radius(&self) -> usize { self.spawn_protection }

    pub fn set_view_distance(&mut self, radius: usize) {
        self.set_value("view-distance", radius);
        self.view_distance = radius;
    }

    pub fn view_distance(&self) -> usize { self.view_distance }

    pub fn set_max_tick_time(&mut self, ticks: isize) {
        self.set_value("max-tick-time", ticks);
        self.max_tick_time = ticks;
    }

    pub fn max_tick_time(&self) -> isize { self.max_tick_time }

    pub fn save(&self) {
        let prop_file = File::create(&self.path).unwrap();
        java_properties::write(BufWriter::new(prop_file), &self.raw_properties).unwrap();
    }
}

impl Display for ServerProperties {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        for (key, val) in self.raw_properties.iter() {
            let disp = get_disp_title(key);
            writeln!(f, "{disp} {val}")?;
        }
        Ok(())
    }
}

fn get_disp_title(key: &str) -> ColoredString {
    match key {
        "allow-flight" => "Allow Flight:".bright_white(),
        "hardcore" => "Hardcore Mode:".bright_white(),
        "difficulty" => "Difficulty:".bright_white(),
        "enable-command-block" => "Enable Command Blocks:".bright_white(),
        "gamemode" => "Game Mode:".bright_white(),
        "level-seed" => "World Seed:".bright_white(),
        "level-type" => "World Type:".bright_white(),
        "max-players" => "Max Players:".bright_white(),
        "motd" => "Server Message:".bright_white(),
        "player-idle-timeout" => "Player Idle Timeout:".bright_white(),
        "pvp" => "PVP:".bright_white(),
        "query.port" => "Query Port:".bright_white(),
        "server-port" => "Port:".bright_white(),
        "spawn-animals" => "Spawn Animals:".bright_white(),
        "spawn-monsters" => "Spawn Monsters:".bright_white(),
        "spawn-npcs" => "Spawn Villagers:".bright_white(),
        "spawn-protection" => "Spawn Protection Radius:".bright_white(),
        "view-distance" => "View Distance:".bright_white(),
        "max-tick-time" => "Max Tick Time:".bright_white(),
        "allow-nether" => "Allow Nether Dimension:".bright_white(),
        "broadcast-console-to-ops" => "Broadcast Console to Operators:".bright_white(),
        "broadcast-rcon-to-ops" => "Broadcast Remote Console to Operators:".bright_white(),
        "enable-jmx-monitoring" => "Enable JMX Monitoring:".bright_white(),
        "enable-query" => "Enable Server Querying:".bright_white(),
        "enable-rcon" => "Enable Remote Console:".bright_white(),
        "enable-status" => "Enable Server Status:".bright_white(),
        "enforce-whitelist" => "Enforce Whitelist:".bright_white(),
        "entity-broadcast-range_percentage" => "Entity Broadcast Range:".bright_white(),
        "force-gamemode" => "Force Game Mode:".bright_white(),
        "function-permission-level" => "Function Permission Level:".bright_white(),
        "generate-structures" => "Generate Structures:".bright_white(),
        "generator-settings" => "Generator Settings:".bright_white(),
        "level-name" => "World Name:".bright_white(),
        "max-build-height" => "Max Build Height:".bright_white(),
        "max-world-size" => "Max World Size:".bright_white(),
        "network-compression-threshold" => "Network Compression Threshold:".bright_white(),
        "online-mode" => "Online Mode:".bright_white(),
        "op-permission-level" => "Operator Permission Level:".bright_white(),
        "prevent-proxy-connections" => "Prevent Proxy Connections:".bright_white(),
        "rate-limit" => "Rate Limit:".bright_white(),
        "rcon-password" => "Remote Console Password:".bright_white(),
        "rcon-port" => "Remote Console Port:".bright_white(),
        "resource-pack" => "Resource Pack:".bright_white(),
        "resource-pack-sha1" => "Resource Pack SHA1 Hash:".bright_white(),
        "server-ip" => "IP Address:".bright_white(),
        "snooper-enabled" => "Snooper Enabled:".bright_white(),
        "sync-chunk-writes" => "Sync Chunk Writes:".bright_white(),
        "text-filtering-config" => "Text Filtering Config:".bright_white(),
        "use-native-transport" => "Use Native Transport:".bright_white(),
        "white-list" => "Whitelist Mode:".bright_white(),
        key => format!("{key} :").bright_white()
    }
}