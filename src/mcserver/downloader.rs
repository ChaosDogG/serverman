use super::Version;
use crate::LOG;
use std::{
    env, fs,
    time::Duration,
    thread::{self, JoinHandle}
};
use glob::glob;
use regex::Regex;
use lazy_static::lazy_static;
use reqwest::blocking::{ClientBuilder, Client};
use rust_utils::logging::LogLevel;

pub type DownloadThread = JoinHandle<Result<(), String>>;

lazy_static! {
    static ref MC_RE1: Regex = Regex::new(r"https://launcher.mojang.com/v1/objects/[a-z0-9]*/server.jar").expect("Invalid regex!");
    static ref MC_RE2: Regex = Regex::new(r"https://piston-data.mojang.com/v1/objects/[a-z0-9]*/server.jar").expect("Invalid regex!");
    static ref FORGE_URL_RE: Regex = Regex::new(r"https://maven.minecraftforge.net/net/minecraftforge/forge/[0-9\.\-]*/forge[0-9\.\-]*installer.jar").expect("Invalid regex!");
    static ref FORGE_NAME_RE: Regex = Regex::new(r"forge-[0-9\.\-]*installer.jar").expect("Invalid regex!");

    pub static ref MCVERSIONS: String = format!("{}/.local/share/serverman/versions", env::var("HOME").expect("Where the hell is your home folder?"));
    pub static ref FORGE_JARS: String = format!("{}/.local/share/serverman/forge_jars", env::var("HOME").expect("Where the hell is your home folder?"));
    pub static ref FABRIC_JARS: String = format!("{}/.local/share/serverman/fabric_jars", env::var("HOME").expect("Where the hell is your home folder?"));
    pub static ref NCR_JARS: String = format!("{}/.local/share/serverman/ncr_jars", env::var("HOME").expect("Where the hell is your home folder?"));
}

const NCR_FORGE_URL: &str = "https://cdn.modrinth.com/data/qQyHxfxd/versions/8Jd0cqU9/NoChatReports-FORGE-1.19.4-v2.1.0.jar";
const NCR_FABRIC_URL: &str = "https://cdn.modrinth.com/data/qQyHxfxd/versions/6yybObpX/NoChatReports-FABRIC-1.19.4-v2.1.0.jar";
const L4JCFG: &str = include_str!("log4jcfg.txt");

// downloads the server JAR file of the specified Minecraft version
pub fn download_server_jar(version: Version) -> DownloadThread {
    let download_thread = move || {
        let client = gen_client();

        // if the file already exists, do not redownload it
        let server_file = format!("minecraft_server.{version}.jar");
        if let Ok(mcs_file) = fs::read(format!("{}/{server_file}", *MCVERSIONS)) {
            fs::write(&server_file, mcs_file).unwrap();
            return get_l4jconfig(version);
        }

        // get the download link for the specific Minecraft version
        let moj_url = if version.major.is_infinite() {
            // 20w14infinite download link
            "https://launcher.mojang.com/v1/objects/c0711cd9608d1af3d6f05ac423dd8f4199780225/server.jar".to_string()
        }
        else {
            let body = match client.get(format!("https://mcversions.net/download/{version}")).send() {
                Ok(response) => response.text().unwrap(),
                Err(why) => return Err(format!("Unable to connect to mcversions.net! Please check your internet connection! {why}"))
            };

            if let Some(url) = MC_RE1.find(&body) {
                url.as_str().to_string()
            }
            else {
                MC_RE2
                    .find(&body)
                    .expect("Where is the server downloader URL?")
                    .as_str()
                    .to_string()
            }
        };

        LOG.line_basic(format!("Downloading {server_file}"), true);
        let data = match client.get(moj_url).send() {
            Ok(response) => response.bytes().unwrap(),
            Err(why) => return Err(format!("Unable to connect to Mojang's servers! Please check your internet connection! {why}"))
        };

        let version_path = format!("{}/minecraft_server.{}.jar", *MCVERSIONS, version);
        fs::create_dir_all(&*MCVERSIONS).unwrap_or(());
        fs::write(version_path, data).unwrap_or(());
        fs::copy(format!("{}/{server_file}", *MCVERSIONS), &server_file).expect("Server file does not exist!");
        LOG.line_basic("Minecraft server JAR file has finished downloading!", true);
        get_l4jconfig(version)
    };

    thread::spawn(download_thread)
}

// downloads the latest forge installer for a specified version of Minecraft
pub fn download_forge_installer(version: Version, no_chat_reports: bool) -> DownloadThread {
    let download_thread = move || {
        let client = gen_client();
        // install no chat reports if the user wants it
        if version.major == 1.19 && no_chat_reports {
            thread::scope(|s| {
                fs::create_dir("mods").unwrap_or(());
                fs::create_dir_all(&*NCR_JARS).unwrap_or(());
                s.spawn(|| {
                    if let Ok(data) = fs::read(format!("{}/NoChatReports-FORGE-1.19.4-v2.1.0.jar", *NCR_JARS)) {
                        LOG.line_basic("Installing No Chat Reports...", true);
                        fs::write("mods/NoChatReports-FORGE-1.19.4-v2.1.0.jar", data).unwrap();
                    }
                    else {
                        LOG.line_basic("Downloading No Chat Reports...", true);
                        let data = match client.get(NCR_FORGE_URL).send() {
                            Ok(response) => response.bytes().unwrap(),
                            Err(why) => {
                                LOG.line(LogLevel::Error, format!("Unable to connect to modrinth! Please check your internet connection. {why}"), true);
                                return;
                            }
                        };
                        LOG.line_basic("Installing No Chat Reports...", true);
                        fs::write("mods/NoChatReports-FORGE-1.19.4-v2.1.0.jar", &data).unwrap();
                        fs::write(format!("{}/NoChatReports-FORGE-1.19.4-v2.1.0.jar", *NCR_JARS), data).unwrap();
                    }
                });
            });
        }
        // if the file already exists, do not redownload it
        let pattern = format!("{}/forge-{}-*-installer.jar", *FORGE_JARS, version);
        let mut glob = glob(&pattern).expect("Failed to read glob pattern");
        if let Some(path) = glob.next() {
            let name = FORGE_NAME_RE.find(&path.unwrap().into_os_string().into_string().unwrap()).unwrap().as_str().to_string();
            fs::copy(format!("{}/{}", *FORGE_JARS, name), &name).expect("Forge installer file does not exist!");
            return Ok(());
        }

        let forge_url = format!("https://files.minecraftforge.net/net/minecraftforge/forge/index_{version}.html");
        let body = match client.get(forge_url).send() {
            Ok(response) => response.text().unwrap(),
            Err(why) => return Err(format!("Unable to connect to minecraftforge.net! Please check your internet connection. {why}"))
        };

        // the latest forge version should (hopefully) always be the first regex match
        let latest_url = FORGE_URL_RE.find(&body).expect("Where is the forge download URL?").as_str().to_string();
        let name = match FORGE_NAME_RE.find(&latest_url) {
            Some(rmatch) => rmatch.as_str().to_string(),
            None => String::from("forge_latest_installer.jar")
        };
        LOG.line_basic(format!("Downloading {name}"), true);
        
        let data = match client.get(latest_url).send() {
            Ok(response) => response.bytes().unwrap(),
            Err(why) => return Err(format!("Unable to connect to minecraftforge.net! Please check your internet connection. {why}"))
        };

        fs::create_dir_all(&*FORGE_JARS).unwrap_or(());
        fs::write(format!("{}/{name}", *FORGE_JARS), data).unwrap();
        fs::copy(format!("{}/{name}", *FORGE_JARS), &name).expect("Forge installer file does not exist!");
        LOG.line_basic("Forge installer has finished downloading!", true);
        Ok(())
    };

    thread::spawn(download_thread)
}

// downloads and sets up Fabric for a specific Minecraft version
pub fn download_fabric_jar(version: Version, no_chat_reports: bool) -> DownloadThread {
    let download_thread = move || {
        let client = gen_client();
        let fabric_jar = format!("fabric-server-mc.{version}-loader.0.14.17-launcher.0.11.2.jar");
        fs::create_dir("mods").unwrap();

        thread::scope(|s| {
            // just copy the loader file if it already exists
            if let Ok(jarfile) = fs::read(format!("{}/{fabric_jar}", *FABRIC_JARS)) {
                fs::write(&fabric_jar, jarfile).unwrap();
                if let Ok(api_jarfile) = fs::read(format!("{}/fabric-api-{version}.jar", *FABRIC_JARS)) {
                    fs::write(format!("mods/fabric-api-{version}.jar"), api_jarfile).expect("Fabric loader file does not exist!");
                }
            }
            else {
                fs::create_dir_all(&*FABRIC_JARS).unwrap_or(());
                let fabric_url = format!("https://meta.fabricmc.net/v2/versions/loader/{version}/0.14.17/0.11.2/server/jar");
                s.spawn(|| {
                    LOG.line_basic(format!("Downloading {fabric_jar}"), true);
                    let data = match client.get(fabric_url).send() {
                        Ok(response) => response.bytes().unwrap(),
                        Err(why) => {
                            LOG.line(LogLevel::Error, format!("Unable to connect to fabricmc.net! Please check your internet connection. {why}"), true);
                            return;
                        }
                    };
                    fs::write(format!("{}/{fabric_jar}", *FABRIC_JARS), &data).unwrap();
                    fs::write(&fabric_jar, data).expect("Fabric loader file does not exist!");
                });

                s.spawn(|| {
                    let api_data = match client.get(version.fabric_api_url()).send() {
                        Ok(response) => response.bytes().unwrap(),
                        Err(why) => {
                            LOG.line(LogLevel::Error, format!("Unable to connect to modrinth! Please check your internet connection. {why}"), true);
                            return;
                        }
                    };

                    fs::write(format!("{}/fabric-api-{version}.jar", *FABRIC_JARS), &api_data).unwrap();
                    fs::write(format!("mods/fabric-api-{version}.jar"), api_data).expect("Fabric API mod file does not exist!");
                });
            }

            if version.major == 1.19 && no_chat_reports {
                fs::create_dir("mods").unwrap_or(());
                fs::create_dir_all(&*NCR_JARS).unwrap_or(());
                s.spawn(|| {
                    if let Ok(data) = fs::read(format!("{}/NoChatReports-FABRIC-1.19.4-v2.1.0.jar", *NCR_JARS)) {
                        LOG.line_basic("Installing No Chat Reports...", true);
                        fs::write("mods/NoChatReports-FABRIC-1.19.4-v2.1.0.jar", data).unwrap();
                    }
                    else {
                        LOG.line_basic("Downloading No Chat Reports...", true);
                        let data = match client.get(NCR_FABRIC_URL).send() {
                            Ok(response) => response.bytes().unwrap(),
                            Err(why) => {
                                LOG.line(LogLevel::Error, format!("Unable to connect to modrinth! Please check your internet connection. {why}"), true);
                                return;
                            }
                        };
                        LOG.line_basic("Installing No Chat Reports...", true);
                        fs::write("mods/NoChatReports-FABRIC-1.19.4-v2.1.0.jar", &data).unwrap();
                        fs::write(format!("{}/NoChatReports-FABRIC-1.19.4-v2.1.0.jar", *NCR_JARS), data).unwrap();
                    }
                });
            }
        });
        Ok(())
    };

    thread::spawn(download_thread)
}

fn gen_client() -> Client {
    ClientBuilder::new().timeout(None).connect_timeout(Duration::from_secs(30)).build().unwrap()
}

fn get_l4jconfig(version: Version) -> Result<(), String> {
    if version.major >= 1.12 && version.major <= 1.16 {
        fs::write("log4j2_112-116.xml", L4JCFG).unwrap();
    }
    Ok(())
}