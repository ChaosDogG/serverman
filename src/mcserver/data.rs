use std::fmt::{
    Display,
    Formatter,
    Result as FmtResult
};
use serde_derive::{Deserialize, Serialize};

#[macro_export]
macro_rules! version {
    ($major: expr, $minor: expr) => {
        $crate::mcserver::Version {
            major: $major,
            minor: $minor
        }
    };
}

#[derive(Copy, Clone, Deserialize, Serialize)]
pub struct Version {
    pub major: f64,
    pub minor: u8
}

impl Version {
    pub const V20W14INF: Self = version!(f64::INFINITY, 255);

    pub fn fabric_api_url(self) -> &'static str {
        if self == version!(1.14, 4) {
            "https://cdn.modrinth.com/data/P7dR8mSH/versions/0.28.5%2B1.14/fabric-api-0.28.5%2B1.14.jar"
        }
        else if self == version!(1.15, 2) {
            "https://cdn.modrinth.com/data/P7dR8mSH/versions/0.28.5%2B1.15/fabric-api-0.28.5%2B1.15.jar"
        }
        else if self == version!(1.16, 5) {
            "https://cdn.modrinth.com/data/P7dR8mSH/versions/0.42.0%2B1.16/fabric-api-0.42.0%2B1.16.jar"
        }
        else if self == version!(1.17, 1) {
            "https://cdn.modrinth.com/data/P7dR8mSH/versions/0.46.1%2B1.17/fabric-api-0.46.1%2B1.17.jar"
        }
        else if self == version!(1.18, 2) {
            "https://cdn.modrinth.com/data/P7dR8mSH/versions/1cCEN67v/fabric-api-0.75.1%2B1.18.2.jar"
        }
        else if self == version!(1.19, 4) {
            "https://cdn.modrinth.com/data/P7dR8mSH/versions/Pz1hLqTB/fabric-api-0.76.0%2B1.19.4.jar"
        }
        else { "" }
    }
}

impl PartialEq for Version {
    fn eq(&self, other: &Self) -> bool {
        self.major == other.major && self.minor == other.minor
    }
}

impl<'a> From<&'a str> for Version {
    fn from(val: &'a str) -> Version {
        if val == "20w14infinite" { return Self::V20W14INF; }
        let split: Vec<&str> = val.split('.').collect();
        let nums: Vec<u8> = split.iter().map(|s| s.parse().unwrap_or(0)).collect();
        let major = nums[0] as f64 + (nums[1] as f64 / 100.);
        let minor = match nums.get(2) {
            Some(n) => *n,
            None => 0
        };

        Version {
            major,
            minor
        }
    }
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        if self.major.is_infinite() {
            write!(f, "20w14infinite")
        }
        else if self.minor == 0 {
            write!(f, "{}", self.major)
        }
        else {
            write!(f, "{}.{}", self.major, self.minor)
        }
    }
}


#[derive(Deserialize, Serialize, Clone)]
pub struct JavaOptions {
    pub launch_command: String, // command used to launch java
    pub min_mem: usize, // minimum memory in megabytes
    pub max_mem: usize // maximum memory in megabytes
}

impl JavaOptions {
    pub fn default() -> JavaOptions {
        JavaOptions {
            launch_command: String::from("java"),
            min_mem: 1024,
            max_mem: 1024
        }
    }
}

#[derive(Copy, Clone)]
pub enum GameMode {
    Survival,
    Creative,
    Adventure,
    Spectator
}

impl GameMode {
    pub fn from(string: &str) -> GameMode {
        match string.to_lowercase().as_str() {
            "creative" => GameMode::Creative,
            "adventure" => GameMode::Adventure,
            "spectator" => GameMode::Spectator,
            _ => Self::from_int(string.parse().unwrap_or(0))
        }
    }

    pub fn from_int(num: usize) -> GameMode {
        match num {
            0 => GameMode::Survival,
            1 => GameMode::Creative,
            2 => GameMode::Adventure,
            3 => GameMode::Spectator,
            _ => GameMode::Survival
        }
    }
}

impl Display for GameMode {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match *self {
            GameMode::Survival => write!(f, "survival"),
            GameMode::Creative => write!(f, "creative"),
            GameMode::Adventure => write!(f, "adventure"),
            GameMode::Spectator => write!(f, "spectator")
        }
    }
}

#[derive(Copy, Clone)]
pub enum Difficulty {
    Peaceful,
    Easy,
    Normal,
    Hard
}

impl Difficulty {
    pub fn from(string: &str) -> Difficulty {
        match string.to_lowercase().as_str() {
            "peaceful" => Difficulty::Peaceful,
            "easy" => Difficulty::Easy,
            "hard" => Difficulty::Hard,
            _ => Self::from_int(string.parse().unwrap_or(4))
        }
    }

    pub fn from_int(num: usize) -> Difficulty {
        match num {
            0 => Difficulty::Peaceful,
            1 => Difficulty::Easy,
            2 => Difficulty::Normal,
            3 => Difficulty::Hard,
            _ => Difficulty::Normal
        }
    }
}

impl Display for Difficulty {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match *self {
            Difficulty::Peaceful => write!(f, "peaceful"),
            Difficulty::Easy => write!(f, "easy"),
            Difficulty::Normal => write!(f, "normal"),
            Difficulty::Hard => write!(f, "hard")
        }
    }
}

/*#[derive(Clone)]
pub enum WorldType {
    Default,
    Flat,
    LargeBiomes,
    Amplified,
    Buffet,
    Customized, // 1.12.2 only
    QuarkRealistic, // requires quark
    QuarkRealisticLargeBiomes, // requires quark
    BiomesOPlenty, // requires Biomes O'Plenty
    Other(String)
}

impl WorldType {
    #[allow(clippy::match_str_case_mismatch)]
    pub fn from(string: &str) -> WorldType {
        match string.to_lowercase().as_str() {
            "default" | "normal" => WorldType::Default,
            "flat" => WorldType::Flat,
            "largeBiomes" => WorldType::LargeBiomes,
            "buffet" => WorldType::Buffet,
            "customized" => WorldType::Customized,
            "amplified" => WorldType::Amplified,
            "biomesop" | "biomesoplenty"=> WorldType::BiomesOPlenty,
            "quark:realistic" | "realistic" => WorldType::QuarkRealistic,
            "quark:realistic_large_biomes" | "realistic_large_biomes" => WorldType::QuarkRealisticLargeBiomes,
            _ => WorldType::Other(string.to_string())
        }
    }

    pub fn get_string(&self, v: Version) -> String {
        let mut ret = String::from(match *self {
            WorldType::Other(ref string) => string,
            WorldType::Default => {
                if v.major == 1.12 {
                    "NORMAL"    
                }
                else { 
                    "default"
                }
            },

            WorldType::LargeBiomes => "largeBiomes",
            WorldType::Buffet => "buffet",
            WorldType::Amplified => "amplified",
            WorldType::Customized => "customized",
            WorldType::Flat => "flat",
            WorldType::QuarkRealistic => {
                if v.major >= 1.16 {
                    "quark:realistic"
                }
                else {
                    "realistic"
                }
            },

            WorldType::QuarkRealisticLargeBiomes => {
                if v.major >= 1.16 {
                    "quark:realistic_large_biomes"
                }
                else {
                    "realistic_large_biomes"
                }
            },

            WorldType::BiomesOPlenty => {
                if v.major == 1.12 {
                    "BIOMESOP"
                }
                else {
                    "biomesoplenty"
                }
            }
        });

        if v.major == 1.12 {
            ret = ret.to_uppercase();
        }

        ret
    }
}*/