use crate::{
    LOG,
    config::{GlobalConfig, Config},
    client::{
        ClientRequest,
        ConsoleLines,
        SOCKET
    },
    mcserver::{
        TMP_DIR,
        JavaOptions,
        ServerProfile,
        Version
    }
};
use std::{
    fs, thread,
    sync::RwLock,
    fmt::Display,
    process::{self, Command, Stdio},
    io::{BufReader, Write, Read},
    os::unix::net::{UnixStream, UnixListener}
};
use glob::glob;
use rust_utils::logging::LogLevel;
use lazy_static::lazy_static;
use serde::Serialize;

mod instances;
mod importer;

use instances::{Servers, ServerInstance};

const SUCCESS: Result<(), &'static str> = Ok(());

lazy_static! {
    static ref SERVERS: RwLock<Servers> = RwLock::new(Servers::new());
}

#[allow(clippy::manual_flatten)]
pub fn init() {
    LOG.line_basic("Starting up Minecraft Server Manager background process...", true);
    fs::remove_dir_all(&*TMP_DIR).unwrap_or_default();
    fs::create_dir(&*TMP_DIR).expect("Unable to create directiory!");
    let listener = UnixListener::bind(&*SOCKET).unwrap();
    LOG.line_basic("Startup complete!", true);

    // if Ctrl-C is pressed
    ctrlc::set_handler(move || {
        let mut servers = SERVERS.write().unwrap();
        LOG.line_basic("Shutting down!", true);
        servers.cleanup_dead();
        servers.shutdown_all();
        process::exit(0);
    }).expect("Error setting Ctrl-C handler");

    // clean up all dead servers in a background thread
    thread::spawn(move || {
        // sometimes Symeon, infinite loops can actually be useful!
        loop {
            SERVERS.write().unwrap().cleanup_dead();
        }
    });

    for request in listener.incoming() {
        if let Ok(stream) = request {
            thread::spawn(move || exec_req(stream));
        }
    }
}

fn exec_req(stream: UnixStream) {
    let mut out_stream = stream.try_clone().unwrap();
    let buffer = BufReader::new(&stream);
    let encoded: Vec<u8> = buffer.bytes().map(|r| r.unwrap_or(0)).collect();
    let request: ClientRequest = bincode::deserialize(&encoded).expect("Error parsing request!");

    match request {
        ClientRequest::Delete(ref name) => {
            if SERVERS.read().unwrap().is_running(name) {
                report_result(&mut out_stream, Err("Running servers cannot be deleted!"));
                return;
            }

            let mut config = GlobalConfig::load();
            config.remove_profile(name);
            LOG.line_basic(format!("Deleted \"{name}\" Minecraft server"), true);
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::Launch(ref name) => {
            if SERVERS.read().unwrap().is_running(name) {
                report_result(&mut out_stream, Err(format!("{name} is already running!")));
                return;
            }

            let config = GlobalConfig::load();
            let profile = match config.get_profile(name) {
                Some(p) => p,
                None => {
                    report_result(&mut out_stream, Err(format!("Server launch profile \"{name}\" does not exist!")));
                    return;
                }
            };

            SERVERS.write().unwrap().add_server(ServerInstance::launch(profile));
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::Restart(ref name) => {
            LOG.line_basic(format!("Restarting \"{name}\" Minecraft server"), true);
            report_result(&mut out_stream, SERVERS.write().unwrap().restart_server(name))
        }

        ClientRequest::Stop(ref name) => {
            SERVERS.write().unwrap().shut_down_server(name);
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::Console(ref name, num_lines) => {
            let console: String = match SERVERS.read().unwrap().get_console(name) {
                Ok(string) => string,

                Err(error) => {
                    report_result(&mut out_stream, Err(&error));
                    return;
                }
            };

            let mut lines = console.lines();
            let cur_numlines = console.lines().count();

            if num_lines > 0 {
                for _ in 0..num_lines {
                    lines.next();
                }
            }

            let data = if cur_numlines == num_lines {
                ConsoleLines::empty()
            }
            else {
                ConsoleLines{ lines: lines.map(|s| s.to_string()).collect() }
            };

            send_val(&mut out_stream, &data);

        }

        ClientRequest::Rename(ref name, ref new_name) => {
            if SERVERS.read().unwrap().is_running(name) {
                report_result(&mut out_stream, Err("Running servers cannot be renamed!"));
                return;
            }
            let mut config = GlobalConfig::load();
            if let Some(server) = config.get_profile_mut(name) {
                server.rename(new_name);
            }
            config.save().unwrap();
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::ImportServers => {
            importer::import_servers();
            report_result(&mut out_stream, SUCCESS);
        }

        ClientRequest::SetupServer {
            ref name,
            ref java,
            version,
            eula,
            forge,
            fabric,
            no_chat_reports
        } => report_result(&mut out_stream, setup_server(name, java, version, forge, fabric, eula, no_chat_reports)),
        ClientRequest::ServerRunning(ref name) => send_val(&mut out_stream, &SERVERS.read().unwrap().is_running(name)),
        ClientRequest::ExecCmd(ref name, ref cmd) => report_result(&mut out_stream, SERVERS.write().unwrap().exec_command(name, cmd))
    }
}

fn setup_server(name: &str, java: &str, version: Version, forge: bool, fabric: bool, eula: bool, no_chat_reports: bool) -> Result<(), String> {
    let directory = format!("{}/{name}", GlobalConfig::load().default_dir);
    fs::create_dir_all(&directory).unwrap();
    let mut java_ops = JavaOptions::default();
    java_ops.launch_command = java.to_string();
    let new_profile = ServerProfile::new(name, java_ops, version, &directory, forge, fabric);
    LOG.line_basic("Setting up new server", true);
    LOG.line_basic(format!("Name: {}", new_profile.name), true);
    
    if forge {
        LOG.line_basic(format!("Version: {} with Forge", new_profile.version), true);
    }
    else if fabric {
        LOG.line_basic(format!("Version: {} with Fabric", new_profile.version), true);
        if version.major < 1.14 {
            return Err("This version of Minecraft does not support Fabric!".to_string());
        }
    }
    else {
        LOG.line_basic(format!("Version: {}", new_profile.version), true);
    }

    LOG.line_basic(format!("Directory: {}", new_profile.directory), true);
    let profile = new_profile.clone();
    let threads = new_profile.setup_server(eula, no_chat_reports);
    threads.0.join().unwrap().unwrap();

    // get the name of the forge installer JAR and run it
    if forge {
        threads.1.ok_or_else(|| "I swear forge was supposed to be downloading in the background!".to_string())?.join().unwrap().unwrap();
        let pattern = format!("forge-{}-*-installer.jar", profile.version);
        let mut glob = glob(&pattern).expect("Failed to read glob pattern");
        let name = glob.next().unwrap().unwrap().into_os_string().into_string().unwrap();
        let mut forge_setup_cmd = Command::new(profile.java_ops.launch_command);
        let file = fs::File::create("forge_install.log").expect("Unable to create file!");
        let file2 = fs::File::open("forge_install.log").expect("Unable to open file!");
        forge_setup_cmd.arg("-jar").arg(&name).arg("--installServer");
        LOG.line_basic("Installing Forge...", true);
        let proc = forge_setup_cmd.stdout(Stdio::from(file)).stderr(Stdio::from(file2)).spawn().expect("Failed to launch Forge installer!");
        proc.wait_with_output().unwrap();
        fs::remove_file(&name).unwrap_or(());
        fs::remove_file(format!("{}.log", name)).unwrap_or(());
        fs::remove_file("run.sh").unwrap_or(());
        fs::remove_file("run.bat").unwrap_or(());
        fs::remove_file("user_jvm_args.txt").unwrap_or(());
        LOG.line_basic("Forge setup is complete!", true)
    }

    LOG.line_basic(format!("Finished setting up \"{name}\" Minecraft server"), true);
    Ok(())
}

fn report_result<E: Display + Serialize>(stream: &mut UnixStream, result: Result<(), E>) {
    if let Err(error) = result {
        let encoded = bincode::serialize(&error).unwrap();
        stream.write_all(&encoded).expect("Unable to write to socket!");
    }
    else {
        let encoded = bincode::serialize(&()).unwrap();
        stream.write_all(&encoded).expect("Unable to write to socket!");
    }
}

fn send_val<V: serde::Serialize + for <'de> serde::Deserialize<'de> + ?Sized>(stream: &mut UnixStream, val: &V) {
    let encoded = bincode::serialize(val).unwrap();
    if let Err(why) = stream.write_all(&encoded) {
        LOG.line(LogLevel::Warn, format!("Unable to write to socket: {why}"), false);
        LOG.line(LogLevel::Warn, "A console viewer may have crashed or been killed", false);
    };
}