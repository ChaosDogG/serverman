use std::env;
use glob::glob;
use walkdir::WalkDir;
use crate::{
    LOG, version,
    config::{GlobalConfig, Config},
    mcserver::Version
};

pub fn import_servers() {
    LOG.line_basic("Importing Minecraft servers...", true);
    let mut num_servers = 0;
    let mut config = GlobalConfig::load();
    let share_dir = format!("{}/.local/share/serverman",env::var("HOME").unwrap());
    let cfg_dir = format!("{}/.config/serverman",env::var("HOME").unwrap());

    // traverse through every single directory in the default directory, up to a depth of 8
    for dir in WalkDir::new(&config.default_dir).max_depth(8).into_iter().filter_map(Result::ok) {
        let cur_path = dir.path().to_str().unwrap();
        if cur_path.contains(&share_dir) || cur_path.contains(&cfg_dir) { continue; }
        if env::set_current_dir(cur_path).is_ok() {
            // minecraft server names are formatted 2 ways
            let mut mc_glob = glob("minecraft_server.*.jar").unwrap();
            let mut mc_glob2 = glob("minecraft-*-server.jar").unwrap();

            // location of a legacy forge JAR file
            let mut old_forge_glob = glob("forge-*.jar").unwrap();

            // location of the forge launch arguments
            let mut forge_glob = glob("libraries/net/minecraftforge/forge/*/unix_args.txt").unwrap();

            // location of the fabric loader
            let mut fabric_glob = glob("fabric-server-mc.*.jar").unwrap();

            // default name for the Minecraft server (folder name)
            let name = dir.file_name().to_str().unwrap();
            if let Some(Ok(server_path)) = mc_glob.next() {
                let forge = forge_glob.next().is_some() || old_forge_glob.next().is_some();
                let fabric = fabric_glob.next().is_some();
                let server_name = server_path.file_name().unwrap().to_str().unwrap();
                let version = if server_name.contains("20w14infinite") {
                    Version::V20W14INF
                }
                else {
                    let mut name_split = server_name.split('.');
                    let num1 = name_split.nth(1).unwrap().parse::<usize>().unwrap();
                    let num2 = name_split.next().unwrap().parse::<usize>().unwrap();
                    let minor = name_split.next().unwrap_or("0").parse::<u8>().unwrap();
                    let major = num1 as f64 + (num2 as f64 / 100.);
                    version!(major, minor)
                };
                config.import_server(name, cur_path, version, forge, fabric);
                num_servers += 1;
            }
            else if let Some(Ok(server_path)) = mc_glob2.next() {
                let forge = forge_glob.next().is_some() || old_forge_glob.next().is_some();
                let fabric = fabric_glob.next().is_some();
                let server_name = server_path.file_name().unwrap().to_str().unwrap();
                let mut name_split = server_name.split('-');
                let version = Version::from(name_split.nth(1).unwrap());
                config.import_server(name, cur_path, version, forge, fabric);
                num_servers += 1;
            }
        }
    }
    LOG.line_basic(format!("Imported {num_servers} Minecraft servers"), true);
}