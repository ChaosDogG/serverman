use crate::{
    LOG, version,
    mcserver::{ServerProfile, TMP_DIR}
};
use std::{
    env, fs::{self, File}, thread,
    io::Write,
    time::Instant,
    process::{Child, Command, Stdio}
};
use crossbeam_channel::bounded;
use glob::glob;
use rust_utils::{
    utils::run_command,
    logging::LogLevel
};

pub struct Servers { servers: Vec<ServerInstance> }

impl Servers {
    pub fn new() -> Servers {
        Servers {
            servers: vec![]
        }
    }

    pub fn is_running(&self, name: &str) -> bool {
        for server in &self.servers {
            if server.profile.name.as_str() == name {
                return true;
            }
        }
        false
    }

    pub fn get_console(&self, name: &str) -> Result<String, String> {
        let console_str = match fs::read_to_string(format!("{}/{name}.console", &*TMP_DIR)) {
            Ok(string) => string,
            Err(_) => return Err(format!("Unable to find {name} Minecraft server! Is it running?"))
        };

        Ok(console_str)
    }

    pub fn add_server(&mut self, instance: ServerInstance) {
        self.servers.push(instance);
    }

    pub fn restart_server(&mut self, name: &str) -> Result<(), String> {
        let instance = match self.get_instance(name) {
            Some(i) => i,
            None => return Err(format!("Unable to find \"{name}\" Minecraft server! Is it running?"))
        };

        let profile = instance.profile.clone();
        self.servers.retain(|server| server.profile.name.as_str() != name);
        let new_instance = ServerInstance::launch(&profile);
        self.servers.push(new_instance);
        Ok(())
    }

    pub fn shut_down_server(&mut self, name: &str) {
        self.servers.retain(|server| server.profile.name.as_str() != name);
    }

    pub fn shutdown_all(&mut self) { self.servers.clear(); }

    // drops all the handles to the dead server processes
    pub fn cleanup_dead(&mut self) { self.servers.retain(|server| !server.is_dead()); }

    pub fn exec_command(&mut self, name: &str, cmd: &str) -> Result<(), String> {
        let instance = match self.get_instance_mut(name) {
            Some(i) => i,
            None => return Err(format!("Unable to find \"{name}\" Minecraft server! Is it running?"))
        };

        let stdin = instance.process.as_mut().unwrap().stdin.as_mut().unwrap();
        writeln!(stdin,"{cmd}").expect("Unable to execute command!");
        Ok(())
    }

    // mutable ref to a server instance
    pub fn get_instance_mut(&mut self, name: &str) -> Option<&mut ServerInstance> {
        self.servers.iter_mut().find(|server| server.profile.name.as_str() == name)
    }

    // immutable ref to a server instance
    pub fn get_instance(&mut self, name: &str) -> Option<&ServerInstance> {
        self.servers.iter().find(|&server| server.profile.name.as_str() == name)
    }
}

pub struct ServerInstance {
    process: Option<Child>,
    profile: ServerProfile,
    pid: usize
}

impl ServerInstance {
    pub fn launch(profile: &ServerProfile) -> ServerInstance {
        LOG.line_basic(format!("Launching \"{}\" Minecraft server...", profile.name), true);
        env::set_current_dir(&profile.directory).expect("where is the server directory?");
        let java_ops = profile.java_ops.clone();
        let version = profile.version;
        let mut server_file =  format!("minecraft_server.{}.jar", profile.version);
        if fs::read(&server_file).is_err() {
            server_file = format!("minecraft-{}-server.jar", profile.version);
        }
        let mut launch = Command::new(&java_ops.launch_command);
        launch.arg(format!("-Xmx{}M", java_ops.max_mem)).arg(format!("-Xms{}M", java_ops.min_mem));

        if version.major == 1.17 || (version == version!(1.18, 0)) {
            launch.arg("-Dlog4j2.formatMsgNoLookups=true");
        }
        else if version.major >= 1.12 && version.major <= 1.16 {
            launch.arg("-Dlog4j.configurationFile=log4j2_112-116.xml");
        }

        if profile.forge_modded {
            if version.major >= 1.17 && version.major != 1.7 {
                let mut glob = glob("libraries/net/minecraftforge/forge/*/unix_args.txt").expect("Failed to read glob pattern");
                let forge_arg = format!("@{}", glob.next().unwrap().unwrap().into_os_string().into_string().unwrap());
                launch.arg(&forge_arg);
            }
            else {
                launch.arg("-jar");
                let pattern = format!("forge-{}-*.jar", profile.version);
                let mut glob = glob(&pattern).expect("Failed to read glob pattern");
                let forge_bin = glob.next().unwrap().unwrap().into_os_string().into_string().unwrap();
                launch.arg(&forge_bin);
            }
        }
        else if profile.fabric_modded {
            launch.arg("-jar");
            let mut glob = glob(&format!("fabric-server-mc.{}-*.jar", profile.version)).expect("Failed to read glob pattern");
            let fabric_bin = glob.next().unwrap().unwrap().into_os_string().into_string().unwrap();
            launch.arg(&fabric_bin);
        }
        else {
            launch.arg("-jar");
            launch.arg(&server_file);
        }

        launch.arg("nogui");

        let file = File::create(format!("{}/{}.console", &*TMP_DIR, profile.name)).expect("Unable to create file!");
        let file2 = File::open(format!("{}/{}.console", &*TMP_DIR, profile.name)).expect("Unable to open file!");
        let process = launch.stdout(Stdio::from(file)).stdin(Stdio::piped()).stderr(Stdio::from(file2)).spawn().expect("Failed to launch Minecraft server!");
        let pid = process.id() as usize;

        ServerInstance {
            process: Some(process),
            profile: profile.clone(),
            pid
        }
    }

    // a server is considered dead if it has exited on its own or crashed
    pub fn is_dead(&self) -> bool {
        if env::set_current_dir(format!("/proc/{}", self.pid)).is_ok() {
            // zombies are dead too
            let status = fs::read_to_string("status").unwrap();
            if status.contains("zombie") {
                return true;
            }
            false
        }
        else { true }
    }
}

impl Drop for ServerInstance {
    fn drop(&mut self) {
        if self.is_dead() {
            LOG.line(LogLevel::Warn, format!("\"{}\" Minecraft server died! Did you or an op execute /stop? If not, check your crash reports folder.", self.profile.name), true);
        }
        else {
            LOG.line_basic(format!("Shutting down \"{}\" Minecraft server!", self.profile.name), true);
            run_command("kill", false, ["-2", &self.pid.to_string()]);
        }

        // stop the server in another thread
        let (snd, rcv) = bounded::<()>(0);
        let mut process = self.process.take().unwrap();
        thread::spawn(move || {
            process.wait().expect("Minecraft server wasn't running!");
            snd.send(()).unwrap(); // notify when it is finished
        });

        let timer = Instant::now();
        let mut kill_proc = true;
        while timer.elapsed().as_secs() < 30 {
            // if the server stops in time, don't kill it
            if rcv.try_recv().is_ok() {
                kill_proc = false;
                break;
            }
        }

        // otherwise, fire in the hole!
        if kill_proc {
            run_command("kill", false, ["-9", &self.pid.to_string()]);
            LOG.line(LogLevel::Warn, format!("\"{}\" Minecraft server has been killed.", self.profile.name), true);
            LOG.line(LogLevel::Warn, "It took more than 30 seconds to shut down", true);
        }

        fs::remove_file(format!("{}/{}.console", &*TMP_DIR, self.profile.name)).unwrap_or(());
    }
}