use std::{io, env};
use crate::{
    VERSION, EULA_URL,
    client::{ClientRequest, ServerResult},
    config::{GlobalConfig, Config},
    mcserver::Version,
    tui::console
};
use colored::Colorize;
use cursive_extras::buffered_backend_root;

pub fn parse_args(args: Vec<String>) {
    if args.len() == 1 {
        eprintln!("Please specify an option!");
        return;
    }

    let cmd = args[1].as_str();
    match cmd {
        "launch" => {
            if let Some(name) = args.get(2) {
                launch_server(name);
            }
            else { eprintln!("Please specify a server name!"); }
        }

        "restart" | "stop" => {
            if let Some(name) = args.get(2) {
                shutdown_server(name, cmd == "restart");
            }
            else { eprintln!("Please specify a server name!"); }
        }

        "console" => {
            if let Some(name) = args.get(2) {
                let mut root = buffered_backend_root();
                console::show(&mut root, name, true);
                root.run();
            }
            else { eprintln!("Please specify a server name!"); }
        }

        "remove" => {
            if let Some(name) = args.get(2) {
                remove_server(name)
            }
            else { eprintln!("Please specify a server name!"); }
        }

        "rename" => {
            if let Some(name) = args.get(2) {
                if let Some(new_name) = args.get(3) {
                    println!("Renaming \"{name}\" Minecraft server...");
                    match ClientRequest::Rename(name.to_string(), new_name.to_string()).send_empty().result {
                        ServerResult::Success => println!("\"{name}\" is now know as \"{new_name}\""),
                        ServerResult::Fail(error) => eprintln!("Unable to rename \"{name}\" Minecraft server! {error}")
                    }
                }
                else { eprintln!("Please specify a new server name!"); }
            }
            else { eprintln!("Please specify a server name!"); }
        }

        "servers" => {
            let config = GlobalConfig::load();
            println!("{}\n", "Known Minecraft servers:".bright_white());

            for server in config.profiles {
                println!("{server}");
            }
        }

        "version" | "-v" => {
            println!("Minecraft Server Manager");
            println!("Version {VERSION}");
        }

        "help" | "-h" => {
            println!("{}","Minecraft Server Manager Help:".bright_white());
            println!("{}", "Subcommands:".bright_white());
            println!("    {} set up a new Minecraft server","setup:".bright_white());
            println!("    {} set up a new server with Minecraft Forge installed","setup-forge:".bright_white());
            println!("    {} Launch a specified server", "launch <server name>:".bright_white());
            println!("    {} Restart a specified server", "restart <name>:".bright_white());
            println!("    {} Show the console of a specified server", "console <name>:".bright_white());
            println!("    {} Deletes a specified server. Please use this carefully because it cannot be undone!", "remove <name>:".bright_white());
            println!("    {} Renames a specified server.", "rename <name> <new name>:".bright_white());
            println!("    {} Get or set the configuration (server.properties) of a specified server", "config <name> [option] [key] [new value]:".bright_white());
            println!("    {} List all known Minecraft servers", "servers:".bright_white());
            println!("    {} Import all servers not created by this program", "import:".bright_white());
            println!("    {} show this message again", "help:".bright_white());
        }

        "import" => {
            println!("Importing Minecraft servers...");
            ClientRequest::ImportServers.send_empty();
        }

        "setup" => setup_server(false, false),
        "setup-forge" => setup_server(true, false),
        "setup-fabric" => setup_server(false, true),
        "config" => get_server_cfg(args.get(2), args.get(3), args.get(4), args.get(5)),
        _ => println!("Invalid option!")
    }
}

fn get_server_cfg(name: Option<&String>, subcmd: Option<&String>, key: Option<&String>, new_val: Option<&String>) {
    if name.is_none() {
        eprintln!("Please specify a server name!");
        return;
    }
    let name = name.unwrap().as_str();

    if name == "keys" {
        println!("{}\n\
        allow-flight\n\
        allow-nether\n\
        broadcast-console-to-ops\n\
        broadcast-rcon-to-ops\n\
        difficulty\n\
        enable-command-block\n\
        enable-jmx-monitoring\n\
        enable-query\n\
        enable-rcon\n\
        enable-status\n\
        enforce-whitelist\n\
        entity-broadcast-range-percentage\n\
        force-gamemode\n\
        function-permission-level\n\
        gamemode\n\
        generate-structures\n\
        generator-settings\n\
        hardcore\n\
        level-name\n\
        level-seed\n\
        level-type\n\
        max-build-height\n\
        max-players\n\
        max-tick-time\n\
        max-world-size\n\
        motd\n\
        network-compression-threshold\n\
        online-mode\n\
        op-permission-level\n\
        player-idle-timeout\n\
        prevent-proxy-connections\n\
        pvp\n\
        query.port\n\
        rate-limit\n\
        rcon.password\n\
        rcon.port\n\
        resource-pack\n\
        resource-pack-sha1\n\
        server-ip\n\
        server-port\n\
        snooper-enabled\n\
        spawn-animals\n\
        spawn-monsters\n\
        spawn-npcs\n\
        spawn-protection\n\
        sync-chunk-writes\n\
        text-filtering-config\n\
        use-native-transport\n\
        view-distance\n\
        white-list", "List of all known server configuration (server.properties) keys:".bright_white());
        return;
    }
    else if name == "help" {
        println!("{}", "Minecraft server configuration help:".bright_white());
        println!("{}", "Subcommands:".bright_white());
        println!("    {} sets a config key to a new value", "<name> set <key name> <new value>:".bright_white());
        println!("    {} get a config key value", "<name> get <key name>:".bright_white());
        println!("    {} list all known config keys", "keys:".bright_white());
        println!("    {} show this message again", "help:".bright_white());
        return;
    }

    let config = GlobalConfig::load();

    let profile = match config.get_profile(name) {
        Some(server) => server,
        None => {
            eprintln!("Error retrieving the configuration of \"{name}\" Minecraft server: Server does not exist!");
            return;
        }
    };

    env::set_current_dir(&profile.directory).expect("Where is the directory for the current server?");
    let properties = profile.get_properties();
    if let Some(s) = subcmd {
        if key.is_none() {
            println!("Please specify a config key!");
            return;
        }
        let key = key.unwrap();

        if s.as_str() == "get" {
            if let Some(disp) = properties.get_disp_value(key) {
                println!("{disp}");
            }
        }
        else if s.as_str() == "set" {
            if new_val.is_none() {
                eprintln!("Please specify a new value for the config key!")
            }

            ClientRequest::ExecCmd(name.to_string(), "reload".to_string()).send_empty();
        }
        else if s.as_str() == "help" { }
        else { eprintln!("Invalid option!"); }
    }
    else {
        println!("{}{}{}", "Configuration for \"".bright_white(), name.bright_white(), "\" Minecraft server:".bright_white());
        println!("{properties}");
    }
}

fn remove_server(name: &str) {
    let response = ClientRequest::Delete(name.to_string()).send_empty();

    match response.result {
        ServerResult::Success => println!("Removed \"{name}\" Minecraft server"),
        ServerResult::Fail(error) => eprintln!("Error removing \"{name}\" Minecraft server: {error}")
    }
}

fn launch_server(name: &str) {
    let response = ClientRequest::Launch(name.to_string()).send_empty();

    match response.result {
        ServerResult::Success => println!("Successfully launched \"{name}\" Minecraft server"),
        ServerResult::Fail(error) => println!("Error launching \"{name}\" Minecraft server!\n{error}")
    }
}

fn shutdown_server(name: &str, restart: bool) {
    let response = if restart {
        ClientRequest::Restart(name.to_string()).send_empty()
    }
    else {
        ClientRequest::Stop(name.to_string()).send_empty()
    };

    match response.result {
        ServerResult::Success if restart => println!("Successfully restarted \"{name}\" Minecraft server"),
        ServerResult::Success => println!("Successfully shut down \"{name}\" Minecraft server"),
        ServerResult::Fail(why) => eprintln!("Unable to {} \"{name}\" Minecraft server: {why}", if restart { "restart" } else { "shut down" })
    }
}

fn setup_server(forge: bool, fabric: bool) {
    if forge { println!("Minecraft Forge will be installed on this server") }
    else if fabric { println!("Fabric mod loader will be installed on this server") }
    let mut eula_res = String::new();
    let mut java = String::new();
    let mut name = String::new();
    let mut version = String::new();

    println!("Do you agree to Mojang's EULA? {EULA_URL} (Please read it)(Y/n)");
    io::stdin().read_line(&mut eula_res).unwrap();

    println!("Name:");
    io::stdin().read_line(&mut name).unwrap();

    println!("Java launch command:");
    io::stdin().read_line(&mut java).unwrap();

    println!("Version:");
    io::stdin().read_line(&mut version).unwrap();

    name.retain(|c| c != '\n');
    java.retain(|c| c != '\n');
    version.retain(|c| c != '\n');
    eula_res.retain(|c| c != '\n');
    let version = Version::from(version.as_str());
    let no_chat_reports = if version.major == 1.19 && (forge || fabric) {
        let mut ncr_res = String::new();
        println!("Install No Chat reports?(Y/n)");
        io::stdin().read_line(&mut ncr_res).unwrap();
        ncr_res.retain(|c| c != '\n');
        ncr_res.to_lowercase().as_str() == "y" || ncr_res.is_empty()
    }
    else { false };

    println!("Setting up your Minecraft server. This may take a little while...");
    let response = ClientRequest::SetupServer {
        name: name.to_string(),
        java: java.to_string(),
        version,
        eula: eula_res.to_lowercase().as_str() == "y" || eula_res.is_empty(),
        forge,
        fabric,
        no_chat_reports
    }
        .send_empty();

    match response.result {
        ServerResult::Success => println!("Successfully set up \"{name}\" Minecraft server"),
        ServerResult::Fail(why) => eprintln!("Error setting up \"{name}\" Minecraft server: {why}")
    }
}