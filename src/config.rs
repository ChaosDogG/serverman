use std::{env, fs};
use serde_derive::{Deserialize, Serialize};
pub use rust_utils::config::Config;
use crate::{
    LOG,
    mcserver::{
        JavaOptions,
        ServerProfile,
        Version
    }
};

#[derive(Deserialize, Serialize)]
pub struct GlobalConfig {
    pub default_dir: String,
    pub profiles: Vec<ServerProfile>
}

impl GlobalConfig {
    pub fn get_profile(&self, name: &str) -> Option<&ServerProfile> {
        self.profiles.iter().find(|p| p.name == name)
    }

    pub fn get_profile_mut(&mut self, name: &str) -> Option<&mut ServerProfile> {
        self.profiles.iter_mut().find(|p| p.name == name)
    }

    pub fn add_profile(&mut self, profile: ServerProfile) {
        self.profiles.push(profile);
        self.save().expect("Unable to save config!");
    }

    pub fn remove_profile(&mut self, name: &str) {
        let profile = match self.get_profile(name) {
            Some(p) => p,
            None => return
        };

        fs::remove_dir_all(&profile.directory).unwrap_or(());
        self.profiles.retain(|profile| profile.name.as_str() != name);
        self.save().expect("Unable to save config!");
    }

    pub fn import_server(&mut self, name: &str, dir: &str, version: Version, forge: bool, fabric: bool) {
        // if the directory being imported is already present, don't import it again
        for profile in &self.profiles {
            if dir == profile.directory { return; }
        }

        LOG.line_basic("Found Minecraft server:", true);

        if forge {
            LOG.line_basic(format!("Version: {version} with Forge"), true);
        }
        else if fabric {
            LOG.line_basic(format!("Version: {version} with Fabric"), true);
        }
        else {
            LOG.line_basic(format!("Version: {version}"), true);
        }
        LOG.line_basic(format!("Directory: {dir}"), true);

        let profile = ServerProfile::new(name, JavaOptions::default(), version, dir, forge, fabric);

        // force MC 1.19.2+ servers to allow clients w/o chat reporting to connect
        if version.major == 1.19 {
            let mut properties = profile.get_properties();
            properties.set_value("enforce-secure-profile", false);
            properties.save();
        }

        self.add_profile(profile);
    }
}

impl Default for GlobalConfig {
    fn default() -> GlobalConfig {
        GlobalConfig {
            default_dir: env::var("HOME").expect("Where the hell is your home folder?"),
            profiles: Vec::new()
        }
    }
}

impl Config for GlobalConfig {
    const FILE_NAME: &'static str = "config.toml";
    fn get_save_dir() -> String {
        format!("{}/.config/serverman/", env::var("HOME").expect("Where the hell is your home folder?"))
    }
}