use std::{env, fs};
use cursive::{
    Cursive,
    traits::{Nameable, Resizable, Finder, With},
    views::{
        SelectView,
        ViewRef,
        EditView,
        TextView,
        HideableView,
        LinearLayout,
        Checkbox,
        Dialog,
        OnEventView,
        ResizedView
    },
    event::{Event, Key},
    view::SizeConstraint
};
use cursive_extras::*;
use crate::{
    EULA_URL, VERSION,
    version,
    config::{GlobalConfig, Config},
    client::{ClientRequest, ServerResult},
    mcserver::{
        ServerProfile,
        Difficulty,
        GameMode,
        JavaOptions,
        Version
    }
};

// general settings dialog
pub fn general_settings(root: &mut Cursive) {
    let cur_dir = GlobalConfig::load().default_dir;

    root.add_layer(
        settings!("General Settings",
            |root| {
                let df_dir_edit = root.find_name::<EditView>("default_dir").unwrap();
                let mut config = GlobalConfig::load();
                config.default_dir = df_dir_edit.get_content().to_string();
                config.save().unwrap();
            },
            TextView::new("Default server folder:"),
            styled_editview(cur_dir, "default_dir", false),
            TextView::new(
                "^ Servers created by \
                Minecraft Server Manager \
                will be placed in this folder. \
                It will be created if it \
                does not exist."
            )
        )
            .fixed_width(35)
    )
}

// server setting dialog
pub fn server_settings(root: &mut Cursive) {
    let mut difficulties = select_view! {
        "Peaceful" => Difficulty::Peaceful,
        "Easy" => Difficulty::Easy,
        "Normal" => Difficulty::Normal,
        "Hard" => Difficulty::Hard
    }.popup();

    let mut game_modes = select_view! {
        "Survival" => GameMode::Survival,
        "Creative" => GameMode::Creative,
        "Adventure" => GameMode::Adventure,
        "Spectator" => GameMode::Spectator
    }.popup();

    let list: ViewRef<SelectView<ServerProfile>> = root.find_name("slist").unwrap();
    if list.selected_id().is_none() { return; }
    let server_id = list.selected_id().unwrap();
    let config = GlobalConfig::load();
    let server = &config.profiles[server_id];
    let properties = server.get_properties();
    difficulties.set_selection(properties.difficulty() as usize);
    game_modes.set_selection(properties.gamemode() as usize);
    let javacmd_in = server.java_ops.launch_command.clone();
    let java_min = server.java_ops.min_mem;
    let java_max = server.java_ops.max_mem;

    root.add_layer(
        settings!(
            format!("{} Settings", server.name),
            move |root| {
                let diff_sv = root.find_name::<SelectView<Difficulty>>("difficulty").unwrap();
                let gm_sv = root.find_name::<SelectView<GameMode>>("gamemode").unwrap();
                let javacmd = root.find_name::<EditView>("javacmd").unwrap();
                let javamin = root.find_name::<EditView>("javamin").unwrap();
                let javamax = root.find_name::<EditView>("javamax").unwrap();
                let max_ticks = root.find_name::<EditView>("max_tick_time").unwrap();
                let max_players = root.find_name::<EditView>("max_players").unwrap();
                let idle_time = root.find_name::<EditView>("player_idle").unwrap();
                let port = root.find_name::<EditView>("port").unwrap();
                let spawn_protection = root.find_name::<EditView>("spawn_protection").unwrap();
                let view_distance = root.find_name::<EditView>("view_distance").unwrap();
                let motd = root.find_name::<EditView>("motd").unwrap();
                let mut config = GlobalConfig::load();
                let server = &mut config.profiles[server_id];
                let mut properties = server.get_properties();
                properties.set_flight(get_checkbox_option(root, "allow_flight"));
                properties.allow_cmd_blocks(get_checkbox_option(root, "cmd_blocks"));
                properties.set_pvp(get_checkbox_option(root, "pvp"));
                properties.set_spawn_animals(get_checkbox_option(root, "animals"));
                properties.set_spawn_monsters(get_checkbox_option(root, "monsters"));
                properties.set_spawn_npcs(get_checkbox_option(root, "npcs"));
                properties.set_hardcore(get_checkbox_option(root, "hardcore"));
                properties.set_gamemode(*gm_sv.selection().unwrap());
                properties.set_difficulty(*diff_sv.selection().unwrap());
                properties.set_max_tick_time(max_ticks.get_content().parse().unwrap_or(-1));
                properties.set_max_players(max_players.get_content().parse().unwrap_or(20));
                properties.set_player_idle_time(idle_time.get_content().parse().unwrap_or(0));
                properties.set_port(port.get_content().parse().unwrap_or(25565));
                properties.set_spawn_protection_radius(spawn_protection.get_content().parse().unwrap_or(16));
                properties.set_view_distance(view_distance.get_content().parse().unwrap_or(10));
                properties.set_message(motd.get_content().as_str());
                properties.save();
                server.java_ops = JavaOptions {
                    launch_command: javacmd.get_content().to_string(),
                    min_mem: javamin.get_content().parse().unwrap_or(1024),
                    max_mem: javamax.get_content().parse().unwrap_or(1024)
                };
                ClientRequest::ExecCmd(server.name.to_string(), "reload".to_string()).send_empty();
                config.save().expect("Unable to save settings!");
                root.pop_layer();
                reload_list(root, false);
            },
            TextView::new("Server Settings:\n"),
            labeled_checkbox("Allow non-Creative mode players to fly", "allow_flight", properties.flight()),
            labeled_checkbox("Allow usage of Command Blocks", "cmd_blocks", properties.cmd_blocks()),
            labeled_checkbox("Allow PVP", "pvp", properties.pvp()),
            labeled_checkbox("Spawn Animals", "animals", properties.spawn_animals()),
            labeled_checkbox("Spawn Monsters", "monsters", properties.spawn_monsters()),
            labeled_checkbox("Spawn Villagers", "npcs", properties.spawn_npcs()),
            labeled_checkbox("Hardcore Mode", "hardcore", properties.hardcore()),
            hlayout!(
                TextView::new("Difficulty: "),
                difficulties.with_name("difficulty")
            ),
            hlayout!(
                TextView::new("Game Mode: "),
                game_modes.with_name("gamemode")
            ),
            hlayout!(
                TextView::new("Max Tick Time: "),
                styled_editview(properties.max_tick_time(), "max_tick_time", false).fixed_width(7)
            ),
            hlayout!(
                TextView::new("Max Players: "),
                styled_editview(properties.max_players(), "max_players", false).fixed_width(7)
            ),
            hlayout!(
                TextView::new("Player Idle Time: "),
                styled_editview(properties.player_idle_time(), "player_idle", false).fixed_width(7),
                TextView::new(" minutes")
            ),
            hlayout!(
                TextView::new("Server Port: "),
                styled_editview(properties.port(), "port", false).fixed_width(6)
            ),
            hlayout!(
                TextView::new("Spawn Protection Radius: "),
                styled_editview(properties.spawn_protection_radius(), "spawn_protection", false).fixed_width(3),
                TextView::new(" chunks")
            ),
            hlayout!(
                TextView::new("View Distance: "),
                styled_editview(properties.view_distance(), "view_distance", false).fixed_width(3),
                TextView::new(" chunks")
            ),
            hlayout!(
                TextView::new("Server Message: "),
                styled_editview(properties.message(), "motd", false).full_width()
            ),
            TextView::new("\nJava Options:\n"),
            hlayout!(
                TextView::new("Launch command: "),
                styled_editview(javacmd_in, "javacmd", false).fixed_width(15)
            ),
            hlayout!(
                TextView::new("Minimum Memory: "),
                styled_editview(java_min, "javamin", false).fixed_width(7),
                TextView::new(" MB")
            ),
            hlayout!(
                TextView::new("Maximum Memory: "),
                styled_editview(java_max, "javamax", false).fixed_width(7),
                TextView::new(" MB")
            ),
            TextView::new("\nYou may want to restart your server after saving your settings")
        )
            .fixed_width(60)
    );
}

// server renaming dialog
pub fn rename_server(root: &mut Cursive, name: &str, new_name: &str) {
    let new_name = new_name.to_string();
    let name = name.to_string();

    match ClientRequest::Rename(name, new_name).send_empty().result {
        ServerResult::Success => { root.pop_layer(); }
        ServerResult::Fail(why) => root.add_layer(info_dialog("Error", why.to_string()))
    };
    reload_list(root, true);
}

// refresh the server list
pub fn reload_list(root: &mut Cursive, reset: bool) {
    let config = GlobalConfig::load();
    let mut list: ViewRef<SelectView<ServerProfile>> = root.find_name("slist").unwrap();
    let selected = if reset { 0 } else { list.selected_id().unwrap_or(0) };
    list.clear();
    list.add_all(config.profiles
        .iter()
        .map(|server| (&server.name, server.clone()))
    );
    list.set_selection(selected);
    let show_info = !list.is_empty();
    let mut info_view: ViewRef<HideableView<LinearLayout>> = root.find_name("info_view").unwrap();
    info_view.set_visible(show_info);
    let mut info: ViewRef<TextView> = info_view.find_name("info").unwrap();
    if show_info {
        info.set_content(config.profiles[selected].fmt_styled())
    }
    else {
        info.set_content("");
    }
    drop(info);
    drop(info_view);
    drop(list);
    let mut main_layout: ViewRef<ResizedView<LinearLayout>> = root.find_name("main_layout").unwrap();
    if show_info { main_layout.set_height(SizeConstraint::Fixed(11)); }
    else { main_layout.set_height(SizeConstraint::Fixed(0)); }
}

// create new Minecraft server dialog
pub fn create_server(root: &mut Cursive) {
    let versions = select_view! {
        "1.12.2" => version!(1.12, 2),
        "1.14.4" => version!(1.14, 4),
        "1.15.2" => version!(1.15, 2),
        "1.16.5" => version!(1.16, 5),
        "1.17.1" => version!(1.17, 1),
        "1.18.2" => version!(1.18, 2),
        "1.19.4" => version!(1.19, 4),
        "20w14∞" => Version::V20W14INF
    }.popup();

    root.add_layer(
        settings_cb!(
            "New Minecraft Server",
            "Create",
            move |root| {
                let name_box: ViewRef<EditView> = root.find_name("name").unwrap();
                let java_box: ViewRef<EditView> = root.find_name("java").unwrap();
                let versions: ViewRef<SelectView<Version>> = root.find_name("versions").unwrap();
                let name = name_box.get_content().to_string();
                if name.chars().count() == 0 {
                    root.add_layer(info_dialog("Error", "Server name must not be blank!"));
                    return;
                }
                let java = java_box.get_content().to_string();
                let version = *versions.selection().unwrap();
                let forge = get_checkbox_option(root, "forge");
                let fabric = get_checkbox_option(root, "fabric");
                let no_chat_reports = if version.major == 1.19 && (forge || fabric) {
                    let mut ncr_wrapper: ViewRef<HideableView<LinearLayout>> = root.find_name("ncr_wrapper").unwrap();
                    let ncr: ViewRef<Checkbox> = ncr_wrapper.find_name("ncr").unwrap();
                    ncr.is_checked()
                }
                else { false };

                let request = ClientRequest::SetupServer {
                    name,
                    java,
                    version,
                    eula: get_checkbox_option(root, "eula"),
                    forge,
                    fabric,
                    no_chat_reports
                };

                load_resource(root, "Please wait...", "Creating new Minecraft server...",
                    move || request.send_empty(),
                    |root, response| {
                        root.pop_layer();
                        if let ServerResult::Fail(why) = response.result {
                            root.add_layer(info_dialog("Error", format!("Unable to set up Minecraft server: {why}")));
                        }
                        reload_list(root, true);
                    }
                );
            },
            TextView::new(format!("Minecraft EULA Link:\n{EULA_URL}")),
            labeled_checkbox("I agree to the Minecraft EULA", "eula", false),
            hlayout!(
                TextView::new("Server Name: "),
                styled_editview("", "name", false).full_width()
            ),
            hlayout!(
                TextView::new("Java launch command: "),
                styled_editview("java", "java", false).full_width()
            ),
            hlayout!(
                TextView::new("Minecraft Version: "),
                versions.with_name("versions").fixed_width(8)
            ),
            labeled_checkbox_cb("Use Forge", "forge", false, |root, checked| {
                if checked {
                    let mut fabric_box: ViewRef<Checkbox> = root.find_name("fabric").unwrap();
                    fabric_box.set_checked(false);
                }
                let mut ncr_wrapper: ViewRef<HideableView<LinearLayout>> = root.find_name("ncr_wrapper").unwrap();
                let versions: ViewRef<SelectView<Version>> = root.find_name("versions").unwrap();
                let version = *versions.selection().unwrap();
                if version.major == 1.19 {
                    ncr_wrapper.set_visible(checked);
                }
            }),
            labeled_checkbox_cb("Use Fabric", "fabric", false, |root, checked| {
                if checked {
                    let mut forge_box: ViewRef<Checkbox> = root.find_name("forge").unwrap();
                    forge_box.set_checked(false);
                }
                let mut ncr_wrapper: ViewRef<HideableView<LinearLayout>> = root.find_name("ncr_wrapper").unwrap();
                let versions: ViewRef<SelectView<Version>> = root.find_name("versions").unwrap();
                let version = *versions.selection().unwrap();
                if version.major == 1.19 {
                    ncr_wrapper.set_visible(checked);
                }
            }),
            HideableView::new(labeled_checkbox("Install No Chat Reports", "ncr", true))
                .hidden()
                .with_name("ncr_wrapper")
        )
            .fixed_width(60)
    );
}

// help dialog
pub fn help_dialog(root: &mut Cursive) {
    let icon = if fs::read("/usr/share/serverman/icon.png").is_err() { "icon.png" }
    else { "/usr/share/serverman/icon.png" };
    let icon = ImageView::new(10, 5).image(icon);
    let share_dir = format!("{}/.local/share/serverman",env::var("HOME").unwrap());
    let cfg_dir = format!("{}/.config/serverman",env::var("HOME").unwrap());

    root.add_layer(
        Dialog::around(
            hlayout!(
                TextView::new(format!(
                    "Minecraft Server Manager\n\
                    Version {VERSION}\n\n\
                    Settings folder: {cfg_dir}\n\
                    Downloaded files folder: {share_dir}\n\n\
                    Key Shortcuts:\n\
                    q: Quit\n\
                    Esc: Back\n\
                    r: Rename server\n\
                    Del: Delete server (this CANNOT be undone!)"
                )),
                fixed_hspacer(1),
                icon
            )
        )
            .dismiss_button("Back")
            .title("Help")
            .wrap_with(OnEventView::new)
            .on_event(Event::Key(Key::Esc), |r| { r.pop_layer(); })
    )
}