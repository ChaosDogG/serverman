use cursive_extras::*;
use cursive::{
    Cursive,
    views::{
        Dialog,
        TextView,
        SelectView,
        ViewRef,
        Button,
        HideableView,
        EditView,
        LinearLayout,
        OnEventView
    },
    traits::{
        Nameable,
        With,
        Resizable,
        Finder,
        Scrollable
    },
    event::{Event, Key},
};
use crate::{
    config::{GlobalConfig, Config},
    client::{ClientRequest, ServerResult},
    mcserver::ServerProfile
};

pub mod console;
pub mod crash;
mod ui;

pub fn init() {
    let config = GlobalConfig::load();
    
    // server info view
    let server_info = if let Some(p) = config.profiles.get(0) { TextView::new(p.fmt_styled()) } else { TextView::new("") };

    // start/restart selected server
    let start_button = Button::new_raw("Start", |root| {
        let list: ViewRef<SelectView<ServerProfile>> = root.find_name("slist").unwrap();
        if list.selection().is_none() {
            return;
        }
        let server = list.selection().unwrap();
        let name = &server.name;
        if server.is_running() {
            let request = ClientRequest::Restart(name.to_string());
            drop(list);
            load_resource(root, "Please wait...", "Restarting Minecraft server...",
                move || request.send_empty(),
                |root, response| {
                    if let ServerResult::Fail(why) = response.result {
                        root.add_layer(info_dialog("Error", format!("Unable to restart Minecraft server: {why}")));
                    }
                    ui::reload_list(root, false);
                }
            );
        }
        else {
            let response = ClientRequest::Launch(name.to_string()).send_empty();
            if let ServerResult::Fail(why) = response.result {
                root.add_layer(info_dialog("Error", format!("Unable to launch \"{name}\" Minecraft server: {why}")));
            };
            drop(list);
            ui::reload_list(root, false);
        }
    }).with_name("start_button");

    // stop selected server
    let stop_button = Button::new_raw("Stop", |root| {
        let list: ViewRef<SelectView<ServerProfile>> = root.find_name("slist").unwrap();
        if list.selection().is_none() {
            return;
        }
        let server = list.selection().unwrap();
        let name = &server.name;

        let request = ClientRequest::Stop(name.to_string());
        load_resource(root, "Please wait...", "Stopping Minecraft server...",
            move || request.send_empty(),
            |root, response| {
                if let ServerResult::Fail(error) = response.result {
                    root.add_layer(info_dialog("Error", format!("Unable to stop Minecraft server: {error}")));
                }
                ui::reload_list(root, false);
            }
        );
    });

    // adjust the settings of selected server
    let cfg_button = Button::new_raw("Settings", ui::server_settings);

    let show_info = !config.profiles.is_empty();
    let mut root = buffered_backend_root();
    root.set_theme(better_theme());
    root.add_layer(
        Dialog::around(
            hlayout!(
                // server list
                SelectView::new()
                    .with_all(config.profiles
                        .iter()
                        .map(|server| (&server.name, server.clone()))
                    )
                    .on_select(|root, profile| {
                        let mut info_view: ViewRef<HideableView<LinearLayout>> = root.find_name("info_view").unwrap();
                        let mut info: ViewRef<TextView> = info_view.find_name("info").unwrap();
                        info.set_content(profile.fmt_styled());
                    })
                    .with_name("slist")
                    .wrap_with(OnEventView::new)
                    .on_event(Key::Del, |root| {
                        let list: ViewRef<SelectView<ServerProfile>> = root.find_name("slist").unwrap();
                        let server = list.selection().unwrap();
                        let name = server.name.to_string();
                        root.add_layer(
                            confirm_dialog("Delete Server", "Are you sure? This cannot be undone!", move |root| {
                                let response = ClientRequest::Delete(name.to_string()).send_empty();
                                match response.result {
                                    ServerResult::Success => {
                                        root.pop_layer();
                                        ui::reload_list(root, true);
                                    }

                                    ServerResult::Fail(why) => root.add_layer(info_dialog("Error", why.to_string()))
                                };
                            })
                        );
                    })
                    .on_event('r', |root| {
                        let list: ViewRef<SelectView<ServerProfile>> = root.find_name("slist").unwrap();
                        let server = list.selection().unwrap();
                        let name = server.name.to_string();
                        let name2 = name.clone();
                        let mut rename_edit = styled_editview(&name, "new_name", false);
                        rename_edit.get_mut()
                            .set_on_submit(move |root, new_name| ui::rename_server(root, &name, new_name));
                        root.add_layer(
                            Dialog::around(
                                hlayout!(
                                    TextView::new("New name: "),
                                    rename_edit.fixed_width(50)
                                )
                            )
                                .button("Rename", move |root| {
                                    let rn_edit = root.find_name::<EditView>("new_name").unwrap();
                                    let new_name = rn_edit.get_content();
                                    ui::rename_server(root, &name2, &new_name);
                                })
                                .dismiss_button("Back")
                                .title("Rename Server")
                                .wrap_with(OnEventView::new)
                                .on_event(Event::Key(Key::Esc), |root| { root.pop_layer(); })
                        )
                    })
                    .scrollable(),

                HDivider::new(),
                HideableView::new(
                    vlayout!(
                        server_info.with_name("info"),
                        hlayout!(
                            start_button,
                            fixed_hspacer(1),
                            stop_button
                        ),
                        hlayout!(
                            cfg_button,
                            fixed_hspacer(1),

                            // view console of selected server
                            Button::new_raw("Console", |root| {
                                let list: ViewRef<SelectView<ServerProfile>> = root.find_name("slist").unwrap();
                                if list.selection().is_none() {
                                    return;
                                }
                                let name = &list.selection().unwrap().name;
                                console::show(root, name, false);
                            })
                        ),

                        // crash reports
                        Button::new_raw("Crash Reports", |root| {
                            let list: ViewRef<SelectView<ServerProfile>> = root.find_name("slist").unwrap();
                            let server = list.selection().unwrap();
                            crash::show_reports(&server, root);
                        })
                            .fixed_width(13)
                    )
                )
                    .visible(show_info)
                    .with_name("info_view")
            )
                .fixed_height(if show_info { 11 } else { 0 })
                .with_name("main_layout")
        )
            .title("Minecraft Server Manager")
            .button("New Server", ui::create_server)
            .button("Import Servers", |root| {
                load_resource(root, "Please wait...", "Importing Minecraft servers...",
                    || ClientRequest::ImportServers.send_empty(),
                    |root, _| {
                        ui::reload_list(root, true);
                        root.add_layer(
                            info_dialog("Import Servers",
                                "Server import complete. \
                                You may want to check the java \
                                settings of each server to make \
                                sure they are correct."
                            )
                                .fixed_width(30)
                        );
                    }
                );
            })
            .button("Quit", Cursive::quit)
            .button("Help", ui::help_dialog)
            .button("Settings", ui::general_settings)
    );

    root.add_global_callback(Event::Refresh, |root| {
        let list: ViewRef<SelectView<ServerProfile>> = root.find_name("slist").unwrap();
        let mut start_button: ViewRef<Button> = root.find_name("start_button").unwrap();
        if list.selected_id().is_none() {
            return;
        }
        let server = list.selection().unwrap();
        if server.is_running() {
            start_button.set_label_raw("Restart");
        }
        else {
            start_button.set_label_raw("Start");
        }
    });

    root.add_global_callback('q', Cursive::quit);
    root.set_fps(30);
    root.run();
}