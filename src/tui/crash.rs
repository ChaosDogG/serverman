use cursive::{
    Cursive, With,
    event::Key,
    traits::Scrollable,
    views::{Dialog, OnEventView, SelectView, TextView}
};
use std::{env, fs};
use glob::glob;
use crate::mcserver::ServerProfile;

// show the crash reports UI
pub fn show_reports(profile: &ServerProfile, root: &mut Cursive) {
    if env::set_current_dir(format!("{}/crash-reports/", profile.directory)).is_err() {
        return;
    }

    let dates = get_report_dates();
    if dates.is_empty() {
        return;
    }
    let mut dates_view = SelectView::new();
    for date in dates {
        dates_view.add_item(format!("{}/{}/{} {}:{:02}", date.1, date.2, date.0, date.3, date.4), date.5)
    }

    dates_view.set_on_submit(|root: &mut Cursive, file: &String| {
        let crash_file = fs::read_to_string(file).unwrap();
        root.add_fullscreen_layer(
            TextView::new(crash_file)
                .scrollable()
                .wrap_with(OnEventView::new)
                .on_event(Key::Esc, |v| { v.pop_layer(); })
        )
    });

    root.add_layer(
        Dialog::around(dates_view)
            .title("Crash Reports")
            .dismiss_button("Back")
            .wrap_with(OnEventView::new)
            .on_event(Key::Esc, |v| { v.pop_layer(); })
    )
}

// get a list of crash report dates
fn get_report_dates() -> Vec<(u32, u32, u32, u32, u32, String)> {
    let mut dates = Vec::new();
    let glob = glob("*").expect("Failed to read glob pattern");
    for file in glob {
        let report_name = file.unwrap().display().to_string();
        let mut split = report_name.split('-');
        let year = split.nth(1).unwrap().parse::<u32>().unwrap();
        let month = split.next().unwrap().parse::<u32>().unwrap();
        let daytime_str = split.next().unwrap();
        let mut split2 = daytime_str.split('_');
        let day = split2.next().unwrap().parse::<u32>().unwrap();
        let time = split2.next().unwrap();
        let mut split3 = time.split('.');
        let hour = split3.next().unwrap().parse::<u32>().unwrap();
        let minute = split3.next().unwrap().parse::<u32>().unwrap();
        dates.push((year, month, day, hour, minute, report_name));
    }

    dates
}