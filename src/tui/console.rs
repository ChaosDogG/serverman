use cursive_extras::*;
use cursive::{
    Cursive, With,
    utils::markup::StyledString,
    theme::{Color, BaseColor, ColorStyle, Style},
    views::{
        Button,
        ScrollView,
        NamedView,
        EditView,
        ResizedView,
        OnEventView,
        TextView,
        Dialog,
        HideableView,
        SelectView,
        ViewRef
    },
    event::{Event, Key},
    view::{Scrollable, ScrollStrategy, Nameable}
};
use std::{sync::RwLock};
use lazy_static::lazy_static;
use crate::client::{ClientRequest, ConsoleLines, ServerResult};

lazy_static! {
    static ref NAME: RwLock<String> = RwLock::new(String::new());
}

// show the specified Minecraft server console
pub fn show(root: &mut Cursive, name: &str, quit: bool) {
    let is_running = ClientRequest::ServerRunning(name.to_string()).send::<bool>();
    if !is_running.body.unwrap() {
        return;
    }
    let mut m_name = NAME.write().unwrap();
    *m_name = name.to_string();
    drop(m_name);
    root.set_theme(better_theme());
    
    // command history
    let history: Vec<String> = Vec::new();
    root.set_user_data(history);
    root.set_fps(30);
    let mut cmd_edit = styled_editview_color("", "cmd_input", false, Color::Dark(BaseColor::Black));
    cmd_edit.get_mut()
        .set_on_submit_mut(move |root, cmd| {
            let data = root.user_data::<Vec<String>>().expect("Is the user data not set?");
            data.push(cmd.to_string());
            if cmd == "exit" {
                handle_exit(root, quit);
                return;
            }

            ClientRequest::ExecCmd(NAME.read().unwrap().to_string(), cmd.to_string()).send_empty();
            req_console(root, quit);
        });

    let console = TextView::empty().with_name("console").scrollable().scroll_strategy(ScrollStrategy::StickToBottom).with_name("scroller");
    root.add_fullscreen_layer(
        vlayout!(
            ResizedView::with_full_height(console),
            hlayout!(
                TextView::new(StyledString::styled("> ", ColorStyle::new(Color::Dark(BaseColor::White), Color::Dark(BaseColor::Black)))),
                ResizedView::with_full_width(cmd_edit.with_name("cmd_input")),
                
                // command shortcuts
                Button::new_raw("Actions", |root| {
                    let cmd_list = select_view! {
                        "Teleport Entity" => 0,
                        "Kill Entity" => 1,
                        "Player Management" => 2,
                        "Stop Server" => 3
                    }
                        .on_submit(|root, item| {
                            match *item {
                                // teleport an entity
                                0 => {
                                    root.add_layer(
                                        settings_cb!(
                                            "Teleport Entity",
                                            "Run",
                                            |root| {
                                                let entity = root.find_name::<EditView>("entity").unwrap().get_content();
                                                let dest_entity = root.find_name::<EditView>("dest_entity").unwrap().get_content();
                                                let dest_x = root.find_name::<EditView>("dest_x").unwrap().get_content();
                                                let dest_y = root.find_name::<EditView>("dest_y").unwrap().get_content();
                                                let dest_z = root.find_name::<EditView>("dest_z").unwrap().get_content();
                                                if dest_entity.chars().count() == 0 {
                                                    ClientRequest::ExecCmd(NAME.read().unwrap().to_string(), format!("tp {entity} {dest_x} {dest_y} {dest_z}"))
                                                        .send_empty();
                                                }
                                                else {
                                                    ClientRequest::ExecCmd(NAME.read().unwrap().to_string(), format!("tp {entity} {dest_entity}"))
                                                        .send_empty();
                                                }
                                                root.pop_layer();
                                                root.pop_layer();
                                            },
                                            TextView::new("Entity Selector:"),
                                            styled_editview("", "entity", false),
                                            TextView::new("Destination Entity Selector:"),
                                            styled_editview("", "dest_entity", false),
                                            TextView::new("^ Leave blank to use coordinates instead\nDestination Coordinates:"),
                                            hlayout!(
                                                TextView::new("x: "),
                                                ResizedView::with_fixed_width(5, styled_editview("", "dest_x", false)),
                                                TextView::new("y: "),
                                                ResizedView::with_fixed_width(5, styled_editview("", "dest_y", false)),
                                                TextView::new("z: "),
                                                ResizedView::with_fixed_width(5, styled_editview("", "dest_z", false))
                                            )
                                        )
                                    );
                                }

                                // kill an entity
                                1 => {
                                    root.add_layer(
                                        settings_cb!(
                                            "Kill Entity",
                                            "Run",
                                            |root| {
                                                let entity = root.find_name::<EditView>("entity").unwrap().get_content();
                                                ClientRequest::ExecCmd(NAME.read().unwrap().to_string(), format!("kill {entity}"))
                                                    .send_empty();
                                                root.pop_layer();
                                                root.pop_layer();
                                            },
                                            TextView::new("Entity Selector:"),
                                            styled_editview("", "entity", false)
                                        )
                                    );
                                }

                                // manage a player
                                2 => {
                                    let options = select_view! {
                                        "Pardon" => 0,
                                        "Grant Operator Permissions" => 1,
                                        "Revoke Operator Permissions" => 2,
                                        "Kick" => 3,
                                        "Ban" => 4
                                    }
                                        .popup()
                                        .on_submit(|root, item| {
                                            let mut reason_h = root.find_name::<HideableView<TextView>>("reason_h").unwrap();
                                            let mut reason_h2 = root.find_name::<HideableView<NamedView<EditView>>>("reason_h2").unwrap();
                                            if *item >= 3 {
                                                reason_h.unhide();
                                                reason_h2.unhide();
                                            }
                                            else {
                                                reason_h.hide();
                                                reason_h2.hide();
                                            }
                                        });

                                    root.add_layer(
                                        settings_cb!(
                                            "Player Management",
                                            "Run",
                                            move |root| {
                                                let cmds = ["pardon", "op", "deop", "kick", "ban"];
                                                let player = root.find_name::<EditView>("player").unwrap().get_content();
                                                let options = root.find_name::<SelectView<i32>>("options").unwrap();
                                                let i = *options.selection().unwrap() as usize;
                                                if i >= 3 {
                                                    let reason = root.find_name::<EditView>("reason").unwrap().get_content();
                                                    ClientRequest::ExecCmd(NAME.read().unwrap().to_string(), format!("{} {player} {reason}", cmds[i]))
                                                        .send_empty();
                                                }
                                                else {
                                                    ClientRequest::ExecCmd(NAME.read().unwrap().to_string(), format!("{} {player}", cmds[i]))
                                                        .send_empty();
                                                }
                                                root.pop_layer();
                                                root.pop_layer();
                                            },
                                            TextView::new("Player Name:"),
                                            styled_editview("", "player", false),
                                            HideableView::new(TextView::new("Reason:")).hidden().with_name("reason_h"),
                                            HideableView::new(styled_editview("", "reason", false)).hidden().with_name("reason_h2"),
                                            options.with_name("options")
                                        )
                                    );
                                }

                                // stop the server
                                3 => { ClientRequest::ExecCmd(NAME.read().unwrap().to_string(), "stop".to_string()).send_empty(); }
                                _ => { }
                            }
                        });

                    root.add_layer(
                        Dialog::around(cmd_list)
                            .title("Actions")
                            .dismiss_button("Back")
                            .wrap_with(OnEventView::new)
                            .on_event(Key::Esc, |r| { r.pop_layer(); })
                    )
                }),
                fixed_hspacer(1),

                // command history
                Button::new_raw("History", |root| {
                    let history: &mut Vec<String> = root.user_data::<Vec<String>>().expect("Is the user data not set?");
                    if history.is_empty() {
                        return;
                    }
                    let mut h_list = SelectView::new();
                    for item in history {
                        h_list.add_item(item.to_string(), item.to_string());
                    }
                    h_list.set_on_submit(|root, item: &str| {
                        let mut cmd_edit: ViewRef<EditView> = root.find_name("cmd_input").unwrap();
                        cmd_edit.set_content(item);
                        root.pop_layer();
                    });

                    root.add_layer(
                        Dialog::around(h_list)
                            .title("Command History")
                            .dismiss_button("Back")
                            .wrap_with(OnEventView::new)
                            .on_event(Key::Esc, |r| { r.pop_layer(); })
                    )
                })
            )
        )
        .wrap_with(OnEventView::new)
        .on_event(Event::Refresh, move |r| req_console(r, quit))
        .on_event('q', move |r| handle_exit(r, quit))
        .on_event(Key::Esc, move |r| handle_exit(r, quit))
    );
}

// exit key handler
// should we just exit the console or quit the whole application?
fn handle_exit(root: &mut Cursive, quit: bool) {
    if quit {
        root.quit()
    }
    else {
        root.pop_layer();
    }
}

// console request handler
fn req_console(root: &mut Cursive, quit: bool) {
    let mut console_view: ViewRef<TextView> = root.find_name("console").unwrap();
    let num_lines = console_view.get_content().source().lines().count();
    let response = ClientRequest::Console(NAME.read().unwrap().to_string(), num_lines)
        .send::<ConsoleLines>();
    let body = match response.result {
        ServerResult::Success => response.body.unwrap(),
        
        ServerResult::Fail(_) => {
            handle_exit(root, quit);
            return;
        }
    };

    if body.len() > 0 {
        let mut cur_style = Style::from(Color::Dark(BaseColor::Green));
        for line in body.lines {
            if line.contains("/INFO]") {
                cur_style = Style::from(Color::Dark(BaseColor::Green));
            }
            else if line.contains("/DEBUG]") {
                cur_style = Style::from(Color::Dark(BaseColor::Cyan));
            }
            else if line.contains("/WARN]") {
                cur_style = Style::from(Color::Light(BaseColor::Yellow));
            }
            else if line.contains("/ERROR]") {
                cur_style = Style::from(Color::Light(BaseColor::Red));
            }
            else if line.contains("/FATAL]") {
                cur_style = Style::from(ColorStyle::new(Color::Light(BaseColor::Red), Color::Dark(BaseColor::Black)));
            }
        
            let formatted_line = StyledString::styled(format!("{line}\n"), cur_style);
            console_view.append(formatted_line);
        
            let mut scroller: ViewRef<ScrollView<NamedView<TextView>>> = root.find_name("scroller").unwrap();
            scroller.set_scroll_strategy(ScrollStrategy::StickToBottom);
        }
    }
}