use std::{
    mem,
    os::unix::net::UnixStream,
    net::Shutdown,
    io::{prelude::*, BufReader},
    fmt::{Display, Formatter, Result as FmtResult}
};
use serde_derive::{Deserialize, Serialize};
use lazy_static::lazy_static;
use crate::mcserver::{TMP_DIR, Version};

lazy_static! {
    pub static ref SOCKET: String = format!("{}/socket", *TMP_DIR);
}

#[derive(Serialize, Deserialize, Clone)]
pub struct ConsoleLines {
    pub lines: Vec<String>
}

impl ConsoleLines {
    pub fn empty() -> ConsoleLines {
        ConsoleLines {
            lines: vec![]
        }
    }

    pub fn len(&self) -> usize { self.lines.len() }
}

#[derive(Serialize, Deserialize, Clone)]
pub enum ClientRequest {
    SetupServer {
        name: String,
        java: String,
        version: Version,
        eula: bool,
        forge: bool,
        fabric: bool,
        no_chat_reports: bool
    },

    ServerRunning(String),
    Delete(String),
    Launch(String),
    Restart(String),
    Stop(String),
    Console(String, usize),
    ExecCmd(String, String),
    Rename(String, String),
    ImportServers
}

impl ClientRequest {
    pub fn send<T>(self) -> ServerResponse<T>
        where T: serde::Serialize + for <'de> serde::Deserialize<'de>
    {
        let encoded = bincode::serialize(&self).unwrap();
        let mut response = ServerResponse {
            result: ServerResult::Success,
            body: None
        };

        let mut socket = match UnixStream::connect(&*SOCKET) {
            Ok(s) => s,
            Err(why) => {
                response.result = ServerResult::Fail(ServerError::from(format!("Unable to connect to socket: {why}")));
                return response;
            }
        };

        socket.write_all(&encoded).expect("Unable to write to socket!");
        socket.shutdown(Shutdown::Write).unwrap();
        let buffer = BufReader::new(&socket);
        let encoded: Vec<u8> = buffer.bytes().map(|r| r.unwrap_or(0)).collect();

        if mem::size_of::<T>() == 0 {
            if encoded.len() > 1 {
                match bincode::deserialize::<ServerError>(&encoded) {
                    Ok(se) => response.result = ServerResult::Fail(se),
                    Err(why) => response.result = ServerResult::Fail(ServerError::from(format!("Unable to parse response body: {why}")))
                };
            }
        }
        else {
            match bincode::deserialize::<T>(&encoded) {
                Ok(body) => response.body = Some(body),
                Err(_) => {
                    match bincode::deserialize::<ServerError>(&encoded) {
                        Ok(se) => response.result = ServerResult::Fail(se),
                        Err(why) => response.result = ServerResult::Fail(ServerError::from(format!("Unable to parse response body: {why}")))
                    }
                }
            };
        }
        response
    }

    pub fn send_empty(self) -> ServerResponse<()> { self.send() }
}

#[must_use]
pub enum ServerResult {
    Success,
    Fail(ServerError)
}

#[derive(Serialize, Deserialize)]
pub struct ServerError {
    pub why: String
}

impl From<String> for ServerError {
    fn from(string: String) -> ServerError {
        ServerError { why: string }
    }
}

impl Display for ServerError {
    fn fmt(&self, f: &mut Formatter) -> FmtResult { write!(f, "{}", self.why) }
}

pub struct ServerResponse<T: serde::Serialize + for <'de> serde::Deserialize<'de>> {
    pub result: ServerResult,
    pub body: Option<T>
}